from setuptools import setup, find_packages

setup(
    name="demandware",
    version="18.7",
    description="Demandware XAPI and Bussiness Logic",
    url="https://stash.gpshopper.com/projects/PIP/repos/demandware/browse?at=refs%2Fheads%2Fmaster_18_7",
    author="GPShopper",
    packages=find_packages(exclude=["tests"]),
    install_requires=[
        "nose2",
        "unittest2",
        "pytest",
        "ordereddict",
        "requests",
        "PyJWT",
        "simplejson",
        "six",
        "rauth",
    ],
    zip_safe=False,
)

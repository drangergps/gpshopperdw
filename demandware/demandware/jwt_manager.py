import curlify
import inspect
import jwt
import simplejson
import base64
import requests
import six
import time
import copy

from demandware import faults, resources
from demandware.resources.base import check_fault, get_headers

try:
    from gpshopper import logger
    global_logger = logger.getLogger(__name__)
#If we use this somewhere where gpshopper isn't present...
except:
    import logging
    global_logger = logging.getLogger(__name__)

class JwtToken(object):
    '''
        Helper class for handling the JWT token from demandware
        Expands the components into easy to use bits
        '''

    def __init__(self, encoded_token=None):
        self.raw = encoded_token
        decoded_token = jwt.decode(encoded_token, verify=False)
        self.expire_time = decoded_token.get("exp")
        self.issued_time = decoded_token.get("iat")
        sub = simplejson.loads(decoded_token.get("sub", "{}"))
        self.customer_id = sub.get("customer_info", {}).get("customer_id")
        self.guest = sub.get("customer_info", {}).get("guest")

    def __eq__(self, other):
        return self.raw == other


resource_classes = {
    "baskets": resources.baskets.Api,
    "categories": resources.categories.Api,
    "content": resources.content.Api,
    "content_search": resources.content_search.Api,
    "custom_object": resources.custom_object.Api,
    "customers": resources.customers.Api,
    "gift_certificate": resources.gift_certificate.Api,
    "order_search": resources.order_search.Api,
    "orders": resources.orders.Api,
    "product_lists": resources.product_lists.Api,
    "product_search": resources.product_search.Api,
    "products": resources.products.Api,
    "promotions": resources.promotions.Api,
    "search_suggestions": resources.search_suggestion.Api,
    "sessions": resources.sessions.Api,
    "site": resources.site.Api,
    "stores": resources.stores.Api,
}

class JwtManager(object):

    resource_classes = resource_classes

    def __init__(self, base_url, client_id, session=None, refresh_buffer=1800, username=None, password=None, logger=None, currency=None, locale=None, pretty_print=None):
        self.base_url = base_url
        self.client_id = client_id

        self.session = session or type('session', (object,), {})()
        session_token = getattr(self.session, "demandware", {}).get("jwt_token")
        if session_token:
            self.token = JwtToken(session_token)
        self.refresh_buffer = refresh_buffer

        self.username = username
        self.password = password
        self.currency = currency
        self.locale = locale
        self.pretty_print = pretty_print

        self.logger = logger or global_logger

        for resource_name, resource_class in self.resource_classes.items():
            setattr(self, resource_name, self._resource_wrapper(resource_class))

    @property
    def auth_url(self):
        #TODO: Make version dynamic
        return self.base_url + "/shop/%s/" % "v18_7"

    def auth(self, username='', password=''):
        url = "{0}/customers/auth".format(self.auth_url)
        if username and password:
            headers = get_headers(client_id=self.client_id)
            if six.PY3:
                auth = base64.b64encode(bytes('%s:%s' % (username,password), 'utf-8')).decode("utf-8")
            else:
                auth = base64.b64encode('%s:%s' % (username, password))

            headers["Authorization"] = "Basic %s" % auth
            result = requests.post(url, json={"type": "credentials"}, headers=headers, verify=False)
        else:
            result = requests.post(url, headers=get_headers(client_id=self.client_id), json={"type": "guest"}, verify=False)

        self.logger.debug(
            curlify.to_curl(result.request).replace("-H 'Accept-Encoding: gzip, deflate' ", "")
        )
        check_fault(result)
        return result.headers.get("Authorization","")[len("Bearer "):]


    def refresh_token(self, old_token):
        url = "{0}/customers/auth".format(self.auth_url)
        result = requests.post(url, json={"type":"refresh"}, headers=get_headers(client_id=self.client_id, token=old_token), verify=False)
        check_fault(result)
        return result.headers.get("Authorization", "")[len("Bearer "):]

    def jwt_callback(self, response):
        if response.headers.get("authorization"):
            self.token = JwtToken(response.headers.get("authorization")[len("Bearer "):])

    @property
    def token(self):
        try:
            return self._token
        except (AttributeError):
            raw_token = getattr(self.session, "demandware", {}).get("jwt_token", "")
            if not raw_token:
                raw_token = self.auth(self.username, self.password)
            self.token = JwtToken(raw_token)
            return self.token

    @token.setter
    def token(self, new_token):
        if new_token is None:
            self._token = None
            if getattr(self.session, "demandware", {}).get("jwt_token"):
                self.session.demandware["jwt_token"] = ""
        else:
            self._token = new_token
            if hasattr(self.session, "demandware"):
                self.session.demandware["jwt_token"] = new_token.raw

    def login(self):
        token = JwtToken(self.auth(self.username, self.password))
        #Flush the session AFTER login succeeds
        self.session.demandware = dict() # Flush the session
        self.token = token

    def _resource_wrapper(self, resource_class):

        # Create an instance of resource_class
        resource_instance = resource_class(
            base_url = self.base_url,
            client_id = self.client_id,
            locale=self.locale,
            pretty_print=self.pretty_print,
            currency=self.currency,
            callback=self.jwt_callback,
        )

        def token_wrapper(func):
            def wrapped(*args, **kwargs):
                # Ensure the token is good.
                if self.token:
                    if self.token.expire_time < time.time():
                        self.logger.debug("Token is expired.")
                        if self.token.guest == False and self.username and self.password:
                            self.logger.debug("Attempting to re-auth.")
                            self.token = JwtToken(self.auth(self.username, self.password))
                            self.logger.debug("Successfully re-authed.")
                        else:
                            self.logger.debug("Using a guest customer. Attempting request anyways.")
                    elif self.token.expire_time < (time.time() - self.refresh_buffer):
                        self.logger.debug("Token expires within the refresh buffer.")
                        self.token = JwtToken(self.refresh_token(old_token=self.token.raw))
                        self.logger.debug("Successfully refreshed token.")
                    # Else the token will be used as is
                try:
                    return func(*args, token=self.token.raw if self.token else None, **kwargs)
                except (faults.ExpiredTokenException):
                    self.logger.debug("Attempted to use token, but it was expired.")
                    if self.token.guest == True:
                        self.logger.debug("Using a guest session and the token expired. Clearing session.")
                        self.session.demandware = dict()
                        self.token = None
                        raise
                    elif (not self.username) or (not self.password):
                        self.logger.error("Credentials not provided, token is for a logged in user and the token is expired.")
                        raise
                    else:
                        self.logger.debug("Token was unexpectedly expired. Attempting to re-auth after request.")
                        self.token = JwtToken(self.auth(self.username, self.password))
                        self.logger.debug("Successfully re-authed after request.")
                        return func(*args, token=self.token.raw if self.token else None, **kwargs)
                except (faults.AuthorizationHeaderMissingException): # TokenRequired
                    self.logger.debug("Request requires a token but none was provided.")
                    if self.username and self.password:
                        self.logger.debug("Attempting to auth with provided credentials.")
                        self.token = JwtToken(self.auth(self.username, self.password))
                        self.logger.debug("Successfully authed with provided credentials.")
                    else:
                        self.logger.debug("Dumping current session and authing as a new guest customer.")
                        self.session.demandware = dict()
                        self.token = JwtToken(self.auth())
                        self.logger.debug("Successfully authed as a new guest customer.")
                    return func(*args, token=self.token.raw if self.token else None, **kwargs)

            return wrapped

        def customer_id_wrapper(func, index):
            '''
            A wrapper for substituting in the token customer id IFF the passed in customer_id is empty.
            :param func: Function to be wrapped
            :param index: Position
            :return:
            '''
            def wrapped(*args, **kwargs):
                if self.token:
                    customer_id = self.token.customer_id
                else: #there isn't a token to extract a customer id from.
                    customer_id = None

                if len(args) > index:
                    # customer id was passed as a standard argument
                    args = list(args)
                    args[index] = args[index] or customer_id
                else:
                    # customer id was passed as a keyword argument or wasn't provided
                    kwargs["customer_id"] = kwargs.get("customer_id") or customer_id
                return func(*args, **kwargs)

            return wrapped

        def basket_id_wrapper(func, index):
            '''
            A wrapper for recovering the basket id if the passed id is no longer valid.
            The code beforehand uses the session basket id which is often accurate but is not guaranteed.
            Rather than always pre-empt a check and add calls let's add a recovery here
            :param func: Function to be wrapped
            :return:
            '''
            def wrapped(*args, **kwargs):
                try:
                    #If we got basket id use it. If not use the one in the session.
                    #Ths preserves the original basket id passed into the business layer from kwargs,
                    #Which is used in the logic in the exception handler
                    if not args or not args[index]:
                        #basket id was passed as None with a standard argument
                        if len(args) > index:
                            calling_args = list(copy.copy(args))
                            calling_args[index] = self.session.demandware.get("basket_id")
                            return func(*tuple(calling_args), **kwargs)
                        #basket id was passed as a keyword argument
                        return func(*args,
                            **dict(
                                  kwargs,
                                  **{"basket_id": kwargs.get("basket_id") or self.session.demandware.get("basket_id")}
                            )
                        )
                    else:
                        #basket id was passed as an actual valid with a standard argument
                        return func(*args, **kwargs)
                except faults.BasketNotFoundException as basket_exception:
                    baskets = self.customers.baskets_get(customer_id=self.token.customer_id).baskets
                    # We default to the 0th basket because JWT auth in demandware does not permit multiple baskets.
                    if len(baskets) > 0:
                        self.session.demandware['basket_id'] = baskets[0].basket_id
                    else:
                        self.session.demandware['basket_id'] = self.baskets.post(input={'currency': self.currency}).basket_id

                    if len(args) > index:
                        # basket id was passed as a standard argument.
                        args = list(args)
                        args[index] = self.session.demandware['basket_id']
                    else:
                        if not kwargs['basket_id']:
                            # basket id wasn't provided
                            kwargs["basket_id"] = self.session.demandware['basket_id']
                        else:
                            # In this case the mobile layer should know that we didn't find it when they have passed it explicitly
                            raise basket_exception

                    return func(*args, **kwargs)

            return wrapped


        for name, func in inspect.getmembers(resource_instance, inspect.ismethod):
            arg_list = inspect.getargspec(func).args if six.PY2 else inspect.getfullargspec(func).args
            new_func = func
            if "token" in arg_list and not name.startswith("_"):
            # if arg_list[-1] == "token" and (not name.startswith("_")):
                if "customer_id" in arg_list:
                    new_func = customer_id_wrapper(new_func, arg_list.index("customer_id")-1) # -1 to skip self
                if "basket_id" in arg_list:
                    new_func = basket_id_wrapper(new_func, arg_list.index("basket_id")-1)

                # Token is the outermost wrapper, as its recovery conditions could change the others.
                new_func = token_wrapper(new_func)

                setattr(resource_instance, name, new_func)
            # else leave the function alone
        return resource_instance

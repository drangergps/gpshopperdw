from decimal import Decimal

from core.model import Model, fields

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerProductList(Model):
    @classmethod
    def _build(cls):

        from demandware.models import customerproductlistitem
        from demandware.models import productlistevent
        from demandware.models import customerproductlistitemlink
        from demandware.models import productlistshippingaddress
        from demandware.models import customerproductlistregistrant
        from demandware.models import customeraddresslink

        cls.co_registrant = fields.ModelField(type=customerproductlistregistrant.CustomerProductListRegistrant, default_value=customerproductlistregistrant.CustomerProductListRegistrant())
        cls.creation_date = fields.StringField(required=False)
        cls.current_shipping_address_link = fields.ModelField(type=customeraddresslink.CustomerAddressLink, default_value=customeraddresslink.CustomerAddressLink())
        cls.customer_product_list_items = fields.ListField(type=customerproductlistitem.CustomerProductListItem, default_value=[])
        cls.description = fields.StringField()
        cls.event = fields.ModelField(type=productlistevent.ProductListEvent, default_value=productlistevent.ProductListEvent())
        cls.id = fields.StringField()
        cls.items_link = fields.ModelField(type=customerproductlistitemlink.CustomerProductListItemLink, default_value=customerproductlistitemlink.CustomerProductListItemLink())
        cls.last_modified = fields.StringField(required=False)
        cls.name = fields.StringField()
        cls.post_event_shipping_address_link = fields.ModelField(type=customeraddresslink.CustomerAddressLink, default_value=customeraddresslink.CustomerAddressLink())
        cls.product_list_shipping_address = fields.ModelField(type=productlistshippingaddress.ProductListShippingAddress, default_value=productlistshippingaddress.ProductListShippingAddress())
        cls.public = fields.BoolField()
        cls.registrant = fields.ModelField(type=customerproductlistregistrant.CustomerProductListRegistrant, default_value=customerproductlistregistrant.CustomerProductListRegistrant())
        cls.shipping_address_link = fields.ModelField(type=customeraddresslink.CustomerAddressLink, default_value=customeraddresslink.CustomerAddressLink())
        cls.type = fields.EnumField(values=['wish_list','gift_registry','shopping_list','custom_1','custom_2','custom_2'])

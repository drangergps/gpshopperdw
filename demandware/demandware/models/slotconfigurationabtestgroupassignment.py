from decimal import Decimal

from core.model import Model, fields
from demandware.models import schedule

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SlotConfigurationABTestGroupAssignment(Model):
    @classmethod
    def _build(cls):
        # Helps get around with circular imports in the module level
        cls.abtest_description = fields.StringField()
        cls.abtest_id = fields.StringField()
        cls.enabled = fields.BoolField()
        cls.schedule = fields.ModelField(type=schedule.Schedule)
        cls.segment_description = fields.StringField()
        cls.segment_id = fields.StringField()

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductListLink(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.description = fields.StringField()
        cls.link = fields.StringField()
        cls.name = fields.StringField()
        cls.public = fields.BoolField()
        cls.title = fields.StringField()
        cls.type = fields.StringField()

from decimal import Decimal

from core.model import Model, fields
from demandware.models import orderpaymentcardrequest
from demandware.models import paymentbankaccountrequest

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class BasketPaymentInstrumentRequest(Model):
    """
    https://documentation.demandware.com/DOC1/topic/com.demandware.dochelp/OCAPI/16.9/shop/Documents/BasketPaymentInstrumentRequest.html?resultof=%22%42%61%73%6b%65%74%50%61%79%6d%65%6e%74%49%6e%73%74%72%75%6d%65%6e%74%52%65%71%75%65%73%74%22%20%22%62%61%73%6b%65%74%70%61%79%6d%65%6e%74%69%6e%73%74%72%75%6d%65%6e%74%72%65%71%75%65%73%74%22%20
    """

    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.amount = fields.DecimalField()
        cls.bank_routing_number = fields.StringField()
        cls.customer_payment_instrument_id = fields.StringField()
        cls.gift_certificate_code = fields.StringField()
        cls.payment_bank_account = fields.ModelField(type=paymentbankaccountrequest.PaymentBankAccountRequest, default_value=paymentbankaccountrequest.PaymentBankAccountRequest())
        cls.payment_card = fields.ModelField(type=orderpaymentcardrequest.OrderPaymentCardRequest, default_value=orderpaymentcardrequest.OrderPaymentCardRequest())
        cls.payment_method_id = fields.StringField()

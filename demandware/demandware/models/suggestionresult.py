from decimal import Decimal

from core.model import Model, fields
from demandware.models import suggestion

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SuggestionResult(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.brand_suggestions = fields.ModelField(type=suggestion.Suggestion, default_value=suggestion.Suggestion())
        cls.category_suggestions = fields.ModelField(type=suggestion.Suggestion, default_value=suggestion.Suggestion())
        cls.content_suggestions = fields.ModelField(type=suggestion.Suggestion, default_value=suggestion.Suggestion())
        cls.custom_suggestions = fields.ModelField(type=suggestion.Suggestion, default_value=suggestion.Suggestion())
        cls.product_suggestions = fields.ModelField(type=suggestion.Suggestion, default_value=suggestion.Suggestion())
        cls.query = fields.StringField()

from decimal import Decimal

from core.model import Model, fields
from demandware.models import customerproductlistitem

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerProductListItemResult(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.data = fields.ListField(type=customerproductlistitem.CustomerProductListItem, default_value=[])
        cls.next = fields.StringField()
        cls.previous = fields.StringField()
        cls.start = fields.IntField()

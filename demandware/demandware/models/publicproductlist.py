from decimal import Decimal

from demandware.models import productlistregistrant
from demandware.models import productlistshippingaddress
from demandware.models import productsimplelink
from demandware.models import publicproductlistitem

from core.model import Model, fields
from demandware.models import productlistevent

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PublicProductList(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.co_registrant = fields.ModelField(type=productlistregistrant.ProductListRegistrant, default_value=productlistregistrant.ProductListRegistrant())
        cls.creation_date = fields.StringField(required=False)
        cls.description = fields.StringField()
        cls.event = fields.ModelField(type=productlistevent.ProductListEvent, default_value=productlistevent.ProductListEvent())
        cls.id = fields.StringField()
        cls.items_link = fields.ModelField(type=productsimplelink.ProductSimpleLink, default_value=productsimplelink.ProductSimpleLink())
        cls.last_modified = fields.StringField(required=False)
        cls.name = fields.StringField()
        cls.product_list_items = fields.ListField(type=publicproductlistitem.PublicProductListItem, default_value=[])
        cls.product_list_shipping_address = fields.ModelField(type=productlistshippingaddress.ProductListShippingAddress, default_value=productlistshippingaddress.ProductListShippingAddress())
        cls.public = fields.BoolField()
        cls.registrant = fields.ModelField(type=productlistregistrant.ProductListRegistrant, default_value=productlistregistrant.ProductListRegistrant())
        cls.type = fields.EnumField(values=['wish_list','gift_registry','shopping_list','custom_1','custom_2','custom_2'])

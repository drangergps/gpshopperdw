from decimal import Decimal

from core.model import Model, fields

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SlotConfiguration(Model):
    @classmethod
    def _build(cls):

        from demandware.models import markuptext
        from demandware.models import schedule
        from demandware.models import slotconfigurationassignmentinformation
        from demandware.models import slotcontent

        # Helps get around with circular imports in the module level
        cls.assignment_information = fields.ModelField(type=slotconfigurationassignmentinformation.SlotConfigurationAssignmentInformation)
        cls.callout_msg = fields.ModelField(type=markuptext.MarkupText)
        cls.configuration_id = fields.StringField()
        cls.context = fields.EnumField(values=['global', 'category', 'folder'])
        cls.context_id = fields.StringField()
        cls.creation_date = fields.StringField(required=False)
        cls.customer_groups = fields.ListField(type=String)
        cls.default = fields.BoolField()
        cls.description = fields.StringField()
        cls.enabled = fields.BoolField()
        cls.last_modified = fields.StringField(required=False)
        cls.link = fields.StringField()
        cls.rank = fields.IntField()
        cls.schedule = fields.ModelField(type=schedule.Schedule)
        cls.slot_content = fields.ModelField(type=slotcontent.SlotContent)
        cls.slot_id = fields.StringField()
        cls.template = fields.StringField()
        cls.uuid = fields.StringField()

from decimal import Decimal

from core.model import Model, fields
from demandware.models import product
from demandware.models import productsimplelink

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerProductListItem(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.id = fields.StringField()
        cls.priority = fields.IntField()
        cls.product = fields.ModelField(type=product.Product, default_value=product.Product())
        cls.product_details_link = fields.ModelField(type=productsimplelink.ProductSimpleLink, default_value=productsimplelink.ProductSimpleLink())
        cls.product_id = fields.StringField()
        cls.public = fields.BoolField()
        cls.purchased_quantity = fields.DecimalField()
        cls.quantity = fields.DecimalField()
        cls.type = fields.EnumField(values=['product','product'])

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerInfo(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.customer_id = fields.StringField()
        cls.customer_name = fields.StringField()
        cls.customer_no = fields.StringField()
        cls.email = fields.StringField()

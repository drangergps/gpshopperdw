from decimal import Decimal

from core.model import Model, fields
from demandware.models import discount

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PriceAdjustment(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.applied_discount = fields.ModelField(type=discount.Discount, default_value=discount.Discount())
        cls.coupon_code = fields.StringField()
        cls.created_by = fields.StringField()
        cls.creation_date = fields.StringField(required=False)
        cls.custom = fields.BoolField()
        cls.item_text = fields.StringField()
        cls.last_modified = fields.StringField(required=False)
        cls.manual = fields.BoolField()
        cls.price = fields.DecimalField()
        cls.price_adjustment_id = fields.StringField()
        cls.promotion_id = fields.StringField()
        cls.promotion_link = fields.StringField()
        cls.reason_code = fields.StringField()

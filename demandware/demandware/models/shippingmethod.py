from decimal import Decimal

from core.model import Model, fields
from demandware.models import shippingpromotion

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ShippingMethod(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.description = fields.StringField()
        cls.external_shipping_method = fields.StringField()
        cls.id = fields.StringField()
        cls.name = fields.StringField()
        cls.price = fields.DecimalField()
        cls.shipping_promotions = fields.ListField(type=shippingpromotion.ShippingPromotion, default_value=[])

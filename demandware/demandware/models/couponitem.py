from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CouponItem(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.code = fields.StringField()
        cls.coupon_item_id = fields.StringField()
        cls.status_code = fields.EnumField(values=['coupon_code_already_in_basket','coupon_code_already_redeemed','coupon_code_unknown','coupon_disabled','redemption_limit_exceeded','customer_redemption_limit_exceeded','timeframe_redemption_limit_exceeded','no_active_promotion','coupon_already_in_basket','no_applicable_promotion','applied','applied', None])
        cls.valid = fields.BoolField()

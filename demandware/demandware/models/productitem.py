from decimal import Decimal

from demandware.models import optionitem
from demandware.models import priceadjustment

from core.model import Model, fields
from demandware.models import productlistitemreference

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductItem(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.adjusted_tax = fields.DecimalField()
        cls.base_price = fields.DecimalField()
        cls.bonus_discount_line_item_id = fields.StringField()
        cls.bonus_product_line_item = fields.BoolField()
        cls.gift = fields.BoolField()
        cls.gift_message = fields.StringField()
        cls.inventory_id = fields.StringField()
        cls.item_id = fields.StringField()
        cls.item_text = fields.StringField()
        cls.option_items = fields.ListField(type=optionitem.OptionItem, default_value=[])
        cls.price = fields.DecimalField()
        cls.price_adjustments = fields.ListField(type=priceadjustment.PriceAdjustment, default_value=[])
        cls.price_after_item_discount = fields.DecimalField()
        cls.price_after_order_discount = fields.DecimalField()
        cls.product_id = fields.StringField()
        cls.product_list_item = fields.ModelField(type=productlistitemreference.ProductListItemReference)
        cls.product_name = fields.StringField()
        cls.quantity = fields.DecimalField()
        cls.shipment_id = fields.StringField()
        cls.shipping_item_id = fields.StringField()
        cls.tax = fields.DecimalField()
        cls.tax_basis = fields.DecimalField()
        cls.tax_class_id = fields.StringField()
        cls.tax_rate = fields.DecimalField()
ProductItem.bundled_product_items = fields.ListField(type=ProductItem, default_value=[])
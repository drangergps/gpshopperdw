from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerProductListItemPurchase(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.id = fields.StringField()
        cls.order_no = fields.StringField()
        cls.product_list_item_id = fields.StringField()
        cls.purchaser_name = fields.StringField()
        cls.quantity = fields.DecimalField()

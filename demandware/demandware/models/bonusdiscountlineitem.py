from decimal import Decimal

from core.model import Model, fields
from demandware.models import productdetailslink

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class BonusDiscountLineItem(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.bonus_products = fields.ListField(type=productdetailslink.ProductDetailsLink, default_value=[])
        cls.coupon_code = fields.StringField()
        cls.id = fields.StringField()
        cls.max_bonus_items = fields.IntField()
        cls.promotion_id = fields.StringField()

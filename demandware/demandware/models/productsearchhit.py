from decimal import Decimal

from demandware.models import image
from demandware.models import producttype
from demandware.models import productref

from core.model import Model, fields
from demandware.models import variationattribute

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductSearchHit(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.currency = fields.StringField()
        cls.hit_type = fields.StringField()
        cls.image = fields.ModelField(type=image.Image, default_value=image.Image())
        cls.link = fields.StringField()
        cls.orderable = fields.BoolField()
        cls.price = fields.DecimalField()
        cls.price_max = fields.DecimalField()
        cls.prices = fields.DictField()
        cls.product_id = fields.StringField()
        cls.product_name = fields.StringField()
        cls.product_type = fields.ModelField(type=producttype.ProductType, default_value=producttype.ProductType())
        cls.represented_product = fields.ModelField(type=productref.ProductRef, default_value=productref.ProductRef())
        cls.represented_products = fields.ListField(type=productref.ProductRef, default_value=[])
        cls.variation_attributes = fields.ListField(type=variationattribute.VariationAttribute, default_value=[])

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Note(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.created_by = fields.StringField()
        cls.creation_date = fields.StringField(required=False)
        cls.id = fields.StringField()
        cls.subject = fields.StringField()
        cls.text = fields.StringField()

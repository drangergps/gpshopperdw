from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PublicProductListLink(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.description = fields.StringField()
        cls.link = fields.StringField()
        cls.name = fields.StringField()
        cls.title = fields.StringField()
        cls.type = fields.EnumField(values=['wish_list','gift_registry','shopping_list','custom_1','custom_2','custom_2'])

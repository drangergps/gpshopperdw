import string
from decimal import Decimal

from core.model import Model, fields

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class MarkupText(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.markup = fields.StringField()
        cls.source = fields.StringField()


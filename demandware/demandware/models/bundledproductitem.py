from decimal import Decimal

from core.model import Model, fields
from demandware.models import model

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class BundledProductItem(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.bundled_product_items = fields.ListField(type=model.Model, default_value=[])
        cls.inventory_id = fields.StringField()
        cls.item_text = fields.StringField()
        cls.product_id = fields.StringField()
        cls.product_name = fields.StringField()
        cls.quantity = fields.DecimalField()

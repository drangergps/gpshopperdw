from decimal import Decimal

from core.model import Model, fields

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Campaign(Model):
    @classmethod
    def _build(cls):
        # Helps get around with circular imports in the module level
        cls.campaign_id = fields.StringField()
        cls.coupons = fields.ListField(type=String)
        cls.creation_date = fields.StringField(required=False)
        cls.customer_groups = fields.ListField(type=String)
        cls.description = fields.StringField()
        cls.enabled = fields.BoolField()
        cls.end_date = fields.StringField()
        cls.last_modified = fields.StringField(required=False)
        cls.link = fields.StringField()
        cls.source_code_groups = fields.ListField(type=String)
        cls.start_date = fields.StringField()
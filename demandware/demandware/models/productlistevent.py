from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductListEvent(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.city = fields.StringField()
        cls.country = fields.StringField()
        cls.date = fields.StringField()
        cls.state = fields.StringField()
        cls.type = fields.StringField()

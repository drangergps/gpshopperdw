from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ContentSearchRefinementValue(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.description = fields.StringField()
        cls.hit_count = fields.IntField()
        cls.label = fields.StringField()
        cls.presentation_id = fields.StringField()
        cls.value = fields.StringField()
ContentSearchRefinementValue.values = fields.ListField(type=ContentSearchRefinementValue, default_value=[])
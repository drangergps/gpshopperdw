from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerProductListRegistrant(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.email = fields.StringField()
        cls.first_name = fields.StringField()
        cls.last_name = fields.StringField()
        cls.role = fields.StringField()

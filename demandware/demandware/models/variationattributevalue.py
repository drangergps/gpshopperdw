from decimal import Decimal

from core.model import Model, fields
from demandware.models import image

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class VariationAttributeValue(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.description = fields.StringField()
        cls.image = fields.ModelField(type=image.Image, default_value=image.Image())
        cls.image_swatch = fields.ModelField(type=image.Image, default_value=image.Image())
        cls.name = fields.StringField()
        cls.orderable = fields.BoolField()
        cls.value = fields.StringField()

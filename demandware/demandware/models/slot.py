from decimal import Decimal

from core.model import Model, fields
from demandware.models import slotconfiguration

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Slot(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.context_type = fields.EnumField(values=['global', 'category', 'folder'])
        cls.description = fields.StringField()
        cls.link = fields.StringField()
        cls.preview_url = fields.StringField()
        cls.slot_configurations = fields.ListField(type=slotconfiguration.SlotConfiguration)
        cls.slot_id = fields.StringField()

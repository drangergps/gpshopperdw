from decimal import Decimal

from core.model import Model, fields
from demandware.models import paymentcardspec

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PaymentMethod(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.cards = fields.ListField(type=paymentcardspec.PaymentCardSpec, default_value=[])
        cls.description = fields.StringField()
        cls.id = fields.StringField()
        cls.image = fields.StringField()
        cls.name = fields.StringField()
        cls.payment_processor_id = fields.StringField()

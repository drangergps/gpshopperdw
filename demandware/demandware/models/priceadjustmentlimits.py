from decimal import Decimal

from core.model import Model, fields
from demandware.models import priceadjustmentlimit

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PriceAdjustmentLimits(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.limits = fields.ListField(type=priceadjustmentlimit.PriceAdjustmentLimit, default_value=[])

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomObject(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.key_property = fields.StringField()
        cls.key_value_integer = fields.IntField()
        cls.key_value_string = fields.StringField()
        cls.object_type = fields.StringField()

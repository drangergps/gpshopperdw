from decimal import Decimal

from demandware.models import orderaddress

from core.model import Model, fields
from demandware.models import shippingmethod

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Shipment(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.adjusted_merchandize_total_tax = fields.DecimalField()
        cls.adjusted_shipping_total_tax = fields.DecimalField()
        cls.gift = fields.BoolField()
        cls.gift_message = fields.StringField()
        cls.merchandize_total_tax = fields.DecimalField()
        cls.product_sub_total = fields.DecimalField()
        cls.product_total = fields.DecimalField()
        cls.shipment_id = fields.StringField()
        cls.shipment_no = fields.StringField()
        cls.shipment_total = fields.DecimalField()
        cls.shipping_address = fields.ModelField(type=orderaddress.OrderAddress, default_value=orderaddress.OrderAddress())
        cls.shipping_method = fields.ModelField(type=shippingmethod.ShippingMethod, default_value=shippingmethod.ShippingMethod())
        cls.shipping_status = fields.EnumField(values=['not_shipped','not_shipped'])
        cls.shipping_total = fields.DecimalField()
        cls.shipping_total_tax = fields.DecimalField()
        cls.tax_total = fields.DecimalField()
        cls.tracking_number = fields.StringField()

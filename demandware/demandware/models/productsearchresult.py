from decimal import Decimal

from core.model import Model, fields
from demandware.models import productsearchhit
from demandware.models import productsearchrefinement
from demandware.models import productsearchsortingoption
from demandware.models import suggestion

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductSearchResult(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.count = fields.IntField()
        cls.hits = fields.ListField(type=productsearchhit.ProductSearchHit, default_value=[])
        cls.next = fields.StringField()
        cls.previous = fields.StringField()
        cls.query = fields.StringField()
        cls.refinements = fields.ListField(type=productsearchrefinement.ProductSearchRefinement, default_value=[])
        cls.search_phrase_suggestions = fields.ModelField(type=suggestion.Suggestion, default_value=suggestion.Suggestion())
        cls.select = fields.StringField()
        cls.selected_refinements = fields.DictField()
        cls.selected_sorting_option = fields.StringField()
        cls.sorting_options = fields.ListField(type=productsearchsortingoption.ProductSearchSortingOption, default_value=[])
        cls.start = fields.IntField()
        cls.total = fields.IntField()

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PaymentCardRequest(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.expiration_month = fields.IntField()
        cls.expiration_year = fields.IntField()
        cls.holder = fields.StringField()
        cls.number = fields.StringField()
        cls.security_code = fields.StringField()

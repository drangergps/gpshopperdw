from decimal import Decimal

from demandware.models import bonusdiscountlineitem
from demandware.models import couponitem
from demandware.models import customerinfo
from demandware.models import giftcertificateitem
from demandware.models import orderaddress
from demandware.models import orderpaymentinstrument
from demandware.models import priceadjustment
from demandware.models import productitem
from demandware.models import shipment
from demandware.models import simplelink

from core.model import Model, fields
from demandware.models import shippingitem

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Order(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.adjusted_merchandize_total_tax = fields.DecimalField()
        cls.adjusted_shipping_total_tax = fields.DecimalField()
        cls.billing_address = fields.ModelField(type=orderaddress.OrderAddress, default_value=orderaddress.OrderAddress())
        cls.bonus_discount_line_items = fields.ListField(type=bonusdiscountlineitem.BonusDiscountLineItem, default_value=[])
        cls.channel_type = fields.EnumField(values=['storefront','callcenter','marketplace','dss','store','pinterest','twitter','facebookads','subscriptions','onlinereservation','onlinereservation'])
        cls.confirmation_status = fields.EnumField(values=['not_confirmed','not_confirmed'])
        cls.coupon_items = fields.ListField(type=couponitem.CouponItem, default_value=[])
        cls.created_by = fields.StringField()
        cls.creation_date = fields.StringField(required=False)
        cls.currency = fields.StringField()
        cls.customer_info = fields.ModelField(type=customerinfo.CustomerInfo, default_value=customerinfo.CustomerInfo())
        cls.customer_name = fields.StringField()
        cls.export_status = fields.EnumField(values=['not_exported','exported','ready','ready'])
        cls.external_order_status = fields.StringField()
        cls.gift_certificate_items = fields.ListField(type=giftcertificateitem.GiftCertificateItem, default_value=[])
        cls.last_modified = fields.StringField(required=False)
        cls.merchandize_total_tax = fields.DecimalField()
        cls.notes = fields.ModelField(type=simplelink.SimpleLink, default_value=simplelink.SimpleLink())
        cls.order_no = fields.StringField()
        cls.order_price_adjustments = fields.ListField(type=priceadjustment.PriceAdjustment, default_value=[])
        cls.order_token = fields.StringField()
        cls.order_total = fields.DecimalField()
        cls.payment_instruments = fields.ListField(type=orderpaymentinstrument.OrderPaymentInstrument, default_value=[])
        cls.payment_status = fields.EnumField(values=['not_paid','part_paid','part_paid'])
        cls.product_items = fields.ListField(type=productitem.ProductItem, default_value=[])
        cls.product_sub_total = fields.DecimalField()
        cls.product_total = fields.DecimalField()
        cls.shipments = fields.ListField(type=shipment.Shipment, default_value=[])
        cls.shipping_items = fields.ListField(type=shippingitem.ShippingItem, default_value=[])
        cls.shipping_status = fields.EnumField(values=['not_shipped','part_shipped','part_shipped'])
        cls.shipping_total = fields.DecimalField()
        cls.shipping_total_tax = fields.DecimalField()
        cls.site_id = fields.StringField()
        cls.source_code = fields.StringField()
        cls.status = fields.EnumField(values=['created','new','open','completed','cancelled','replaced','replaced','failed'])
        cls.tax_total = fields.DecimalField()
        cls.taxation = fields.EnumField(values=['gross','net'])

from decimal import Decimal

from core.model import Model, fields
from demandware.models import customerpaymentcardrequest
from demandware.models import paymentbankaccountrequest

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerPaymentInstrumentRequest(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.bank_routing_number = fields.StringField()
        cls.gift_certificate_code = fields.StringField()
        cls.payment_bank_account = fields.ModelField(type=paymentbankaccountrequest.PaymentBankAccountRequest, default_value=paymentbankaccountrequest.PaymentBankAccountRequest())
        cls.payment_card = fields.ModelField(type=customerpaymentcardrequest.CustomerPaymentCardRequest, default_value=customerpaymentcardrequest.CustomerPaymentCardRequest())
        cls.payment_method_id = fields.StringField()

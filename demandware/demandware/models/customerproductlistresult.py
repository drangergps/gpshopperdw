from decimal import Decimal

from core.model import Model, fields
from demandware.models import customerproductlist

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerProductListResult(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.count = fields.IntField()
        cls.data = fields.ListField(type=customerproductlist.CustomerProductList, default_value=[])
        cls.total = fields.IntField()

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class GiftCertificateItem(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.amount = fields.DecimalField()
        cls.gift_certificate_item_id = fields.StringField()
        cls.message = fields.StringField()
        cls.recipient_email = fields.StringField(default_value="")
        cls.recipient_name = fields.StringField(default_value="")
        cls.sender_name = fields.StringField()
        cls.shipment_id = fields.StringField()

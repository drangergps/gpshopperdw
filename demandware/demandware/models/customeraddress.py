from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerAddress(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.address1 = fields.StringField(default_value="")
        cls.address2 = fields.StringField(default_value="")
        cls.address_id = fields.StringField(default_value="")
        cls.city = fields.StringField(default_value="")
        cls.company_name = fields.StringField(default_value="")
        cls.country_code = fields.StringField(default_value="")
        cls.creation_date = fields.StringField(required=False)
        cls.first_name = fields.StringField(default_value="")
        cls.full_name = fields.StringField(default_value="")
        cls.job_title = fields.StringField(default_value="")
        cls.last_modified = fields.StringField(required=False)
        cls.last_name = fields.StringField(default_value="")
        cls.phone = fields.StringField(default_value="")
        cls.post_box = fields.StringField(default_value="")
        cls.postal_code = fields.StringField(default_value="")
        cls.preferred = fields.BoolField(default_value=False)
        cls.salutation = fields.StringField(default_value="")
        cls.second_name = fields.StringField(default_value="")
        cls.state_code = fields.StringField(default_value="")
        cls.suffix = fields.StringField(default_value="")
        cls.suite = fields.StringField(default_value="")
        cls.title = fields.StringField(default_value="")

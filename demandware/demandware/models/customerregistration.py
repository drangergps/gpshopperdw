from decimal import Decimal

from core.model import Model, fields

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class CustomerRegistration(Model):
    @classmethod
    def _build(cls):
        from demandware.models import customer
        cls.customer = fields.ModelField(type=customer.Customer, default_value=customer.Customer())
        cls.password = fields.StringField()

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductLink(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.source_product_id = fields.StringField()
        cls.source_product_link = fields.StringField()
        cls.target_product_id = fields.StringField()
        cls.target_product_link = fields.StringField()
        cls.type = fields.EnumField(values=['cross_sell','replacement','up_sell','accessory','newer_version','alt_orderunit','spare_part','spare_part'])

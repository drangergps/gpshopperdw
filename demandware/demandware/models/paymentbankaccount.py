from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PaymentBankAccount(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.drivers_license_last_digits = fields.StringField()
        cls.drivers_license_state_code = fields.StringField()
        cls.holder = fields.StringField()
        cls.masked_drivers_license = fields.StringField()
        cls.masked_number = fields.StringField()
        cls.number_last_digits = fields.StringField()

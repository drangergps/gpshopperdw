from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductType(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.bundle = fields.BoolField()
        cls.item = fields.BoolField()
        cls.master = fields.BoolField()
        cls.option = fields.BoolField()
        cls.set = fields.BoolField()
        cls.variant = fields.BoolField()
        cls.variation_group = fields.BoolField()

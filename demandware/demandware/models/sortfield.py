from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SortField(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.field_name = fields.StringField()
        cls.sort_order = fields.StringField()

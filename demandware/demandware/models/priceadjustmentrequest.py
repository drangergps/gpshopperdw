from decimal import Decimal

from core.model import Model, fields
from demandware.models import discountrequest

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PriceAdjustmentRequest(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.discount = fields.ModelField(type=discountrequest.DiscountRequest, default_value=discountrequest.DiscountRequest())
        cls.item_id = fields.StringField()
        cls.item_text = fields.StringField()
        cls.level = fields.EnumField(values=['product','shipping','order'])
        cls.promotion_id = fields.StringField()
        cls.reason_code = fields.StringField()

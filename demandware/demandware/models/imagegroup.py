from decimal import Decimal

from demandware.models import image

from core.model import Model, fields
from demandware.models import variationattribute

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ImageGroup(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.images = fields.ListField(type=image.Image, default_value=[])
        cls.variation_attributes = fields.ListField(type=variationattribute.VariationAttribute, default_value=[])
        cls.view_type = fields.StringField()

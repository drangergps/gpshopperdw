from decimal import Decimal

from core.model import Model, fields
from demandware.models import image

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SuggestedProduct(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.currency = fields.StringField()
        cls.image = fields.ModelField(type=image.Image, default_value=image.Image())
        cls.link = fields.StringField()
        cls.price = fields.DecimalField()
        cls.product_id = fields.StringField()
        cls.product_name = fields.StringField()

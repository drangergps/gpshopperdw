from decimal import Decimal

from demandware.models import contentsearchrefinement

from core.model import Model, fields
from demandware.models import content

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ContentSearchResult(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.count = fields.IntField()
        cls.hits = fields.ListField(type=content.Content, default_value=[])
        cls.next = fields.StringField()
        cls.previous = fields.StringField()
        cls.query = fields.StringField()
        cls.refinements = fields.ListField(type=contentsearchrefinement.ContentSearchRefinement, default_value=[])
        cls.selected_refinements = fields.DictField()
        cls.start = fields.IntField()
        cls.total = fields.IntField()

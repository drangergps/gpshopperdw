from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Image(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.alt = fields.StringField()
        cls.dis_base_link = fields.StringField()
        cls.link = fields.StringField()
        cls.title = fields.StringField()

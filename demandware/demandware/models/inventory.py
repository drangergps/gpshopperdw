from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Inventory(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.ats = fields.DecimalField()
        cls.backorderable = fields.BoolField()
        cls.id = fields.StringField()
        cls.in_stock_date = fields.StringField()
        cls.orderable = fields.BoolField()
        cls.preorderable = fields.BoolField()
        cls.stock_level = fields.DecimalField()

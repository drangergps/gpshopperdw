from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductDetailsLink(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.link = fields.StringField()
        cls.product_description = fields.StringField()
        cls.product_id = fields.StringField()
        cls.product_name = fields.StringField()
        cls.title = fields.StringField()

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ShippingPromotion(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.callout_msg = fields.StringField()
        cls.link = fields.StringField()
        cls.promotion_id = fields.StringField()
        cls.promotion_name = fields.StringField()

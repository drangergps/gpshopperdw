from decimal import Decimal

from core.model import Model, fields
from demandware.models import order

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class OrderSearchHit(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.data = fields.ModelField(type=order.Order, default_value=order.Order())
        cls.relevance = fields.DecimalField()

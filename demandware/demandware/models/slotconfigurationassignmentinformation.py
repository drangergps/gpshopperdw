from decimal import Decimal

from core.model import Model, fields
from demandware.models import slotconfigurationabtestgroupassignment
from demandware.models import slotconfigurationcampaignassignnment
from demandware.models import schedule

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SlotConfigurationAssignmentInformation(Model):
    @classmethod
    def _build(cls):
        # Helps get around with circular imports in the module level
        cls.abtest_id = fields.StringField()
        cls.abtest_segment_id = fields.StringField()
        cls.active = fields.BoolField()
        cls.active_abtest_assignments = fields.ListField(type=slotconfigurationabtestgroupassignment.SlotConfigurationABTestGroupAssignment)
        cls.active_campaign_assignments = fields.ListField(type=slotconfigurationcampaignassignnment.SlotConfigurationCampaignAssignment)
        cls.campaign_id = fields.StringField()
        cls.end_date = fields.StringField()
        cls.schedule = fields.ModelField(type=schedule.Schedule)
        cls.schedule_type = fields.EnumField(values=['none', 'campaign', 'abtest', 'multiple'])
        cls.start_date = fields.StringField()
        cls.upcoming_abtest_assignments = fields.ListField(type=slotconfigurationabtestgroupassignment.SlotConfigurationABTestGroupAssignment)
        cls.upcoming_campaign_assignments = fields.ListField(type=slotconfigurationcampaignassignnment.SlotConfigurationCampaignAssignment)
import string
from decimal import Decimal

from demandware.models import suggestedcategory
from demandware.models import suggestedphrase
from demandware.models import suggestedproduct
from demandware.models import suggestedterms

from core.model import Model, fields
from demandware.models import suggestedcontent

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Suggestion(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.brands = fields.ListField(type=string.String, default_value=[])
        cls.categories = fields.ListField(type=suggestedcategory.SuggestedCategory, default_value=[])
        cls.content = fields.ListField(type=suggestedcontent.SuggestedContent, default_value=[])
        cls.custom_suggestions = fields.ListField(type=string.String, default_value=[])
        cls.products = fields.ListField(type=suggestedproduct.SuggestedProduct, default_value=[])
        cls.suggested_phrases = fields.ListField(type=suggestedphrase.SuggestedPhrase, default_value=[])
        cls.suggested_terms = fields.ListField(type=suggestedterms.SuggestedTerms, default_value=[])

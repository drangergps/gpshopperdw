from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class OrderPaymentCardRequest(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.card_type = fields.StringField()
        #This property is in the 18.7 docs, but 18.7 throws an error if it is passed
        #cls.credit_card_expired = fields.BoolField()
        cls.credit_card_token = fields.StringField()
        cls.expiration_month = fields.IntField()
        cls.expiration_year = fields.IntField()
        cls.holder = fields.StringField()
        cls.issue_number = fields.StringField()
        cls.number = fields.StringField()
        cls.security_code = fields.StringField()
        cls.valid_from_month = fields.IntField()
        cls.valid_from_year = fields.IntField()

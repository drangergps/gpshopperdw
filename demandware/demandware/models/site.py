import string
from decimal import Decimal

from core.model import Model, fields
from demandware.models import locale

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Site(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.allowed_currencies = fields.ListField(type=string.String, default_value=[])
        cls.allowed_locales = fields.ListField(type=locale.Locale, default_value=[])
        cls.default_currency = fields.StringField()
        cls.default_locale = fields.StringField()
        cls.http_dis_base_url = fields.StringField()
        cls.http_hostname = fields.StringField()
        cls.http_library_content_url = fields.StringField()
        cls.http_site_content_url = fields.StringField()
        cls.https_dis_base_url = fields.StringField()
        cls.https_hostname = fields.StringField()
        cls.https_library_content_url = fields.StringField()
        cls.https_site_content_url = fields.StringField()
        cls.id = fields.StringField()
        cls.name = fields.StringField()
        cls.status = fields.EnumField(values=['online','online'])
        cls.timezone = fields.StringField()
        cls.timezone_offset = fields.IntField()

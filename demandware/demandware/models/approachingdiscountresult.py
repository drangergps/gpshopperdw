from decimal import Decimal

from core.model import Model, fields
from demandware.models import approachingdiscount

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ApproachingDiscountResult(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.approaching_discounts = fields.ListField(type=approachingdiscount.ApproachingDiscount, default_value=[])

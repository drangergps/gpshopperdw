from decimal import Decimal

from core.model import Model, fields
from demandware.models import slot

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Slots(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.count = fields.IntField()
        cls.data = fields.ListField(type=slot.Slot)
        cls.expand = fields.ListField(type=String)
        cls.next = fields.StringField()
        cls.previous = fields.StringField()
        cls.select = fields.StringField()
        cls.start = fields.IntField()
        cls.total = fields.IntField()
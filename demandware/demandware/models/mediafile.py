import string
from decimal import Decimal

from core.model import Model, fields

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class MediaFile(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.abs_url = fields.StringField()
        cls.alt = fields.StringField()
        cls.dis_base_url = fields.StringField()
        cls.path = fields.StringField()
        cls.title = fields.StringField()

from decimal import Decimal

from core.model import Model, fields
from demandware.models import orderpaymentcardrequest
from demandware.models import paymentbankaccountrequest

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class OrderPaymentInstrumentRequest(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.amount = fields.DecimalField()
        cls.bank_routing_number = fields.StringField()
        cls.create_customer_payment_instrument = fields.BoolField()
        cls.customer_payment_instrument_id = fields.StringField()
        cls.gift_certificate_code = fields.StringField()
        cls.payment_bank_account = fields.ModelField(type=paymentbankaccountrequest.PaymentBankAccountRequest, default_value=paymentbankaccountrequest.PaymentBankAccountRequest())
        cls.payment_card = fields.ModelField(type=orderpaymentcardrequest.OrderPaymentCardRequest, default_value=orderpaymentcardrequest.OrderPaymentCardRequest())
        cls.payment_method_id = fields.StringField()

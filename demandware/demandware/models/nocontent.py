from core.model import Model

class NoContent(Model):
    '''
    An object representing a 204 No Content response. Deliberately contains no fields.
    '''
    pass
from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Master(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.link = fields.StringField()
        cls.master_id = fields.StringField()
        cls.orderable = fields.BoolField()
        cls.price = fields.DecimalField()
        cls.price_max = fields.DecimalField()

from decimal import Decimal

from core.model import Model, fields
from demandware.models import paymentbankaccount
from demandware.models import paymentcard
from demandware.models import status

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class OrderPaymentInstrument(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.amount = fields.DecimalField()
        cls.authorization_status = fields.ModelField(type=status.Status, default_value=status.Status())
        cls.bank_routing_number = fields.StringField()
        cls.masked_gift_certificate_code = fields.StringField()
        cls.payment_bank_account = fields.ModelField(type=paymentbankaccount.PaymentBankAccount, default_value=paymentbankaccount.PaymentBankAccount())
        cls.payment_card = fields.ModelField(type=paymentcard.PaymentCard, default_value=paymentcard.PaymentCard())
        cls.payment_instrument_id = fields.StringField()
        cls.payment_method_id = fields.StringField()

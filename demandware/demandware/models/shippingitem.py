from decimal import Decimal

from core.model import Model, fields
from demandware.models import priceadjustment

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ShippingItem(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.adjusted_tax = fields.DecimalField()
        cls.base_price = fields.DecimalField()
        cls.item_id = fields.StringField()
        cls.item_text = fields.StringField()
        cls.price = fields.DecimalField()
        cls.price_adjustments = fields.ListField(type=priceadjustment.PriceAdjustment, default_value=[])
        cls.price_after_item_discount = fields.DecimalField()
        cls.shipment_id = fields.StringField()
        cls.tax = fields.DecimalField()
        cls.tax_basis = fields.DecimalField()
        cls.tax_class_id = fields.StringField()
        cls.tax_rate = fields.DecimalField()

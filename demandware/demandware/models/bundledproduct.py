from decimal import Decimal

from core.model import Model, fields

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class BundledProduct(Model):
    @classmethod
    def _build(cls):
        from demandware.models import product
    # Helps get around with circular imports in the module level
        cls.product = fields.ModelField(type=product.Product, default_value=product.Product())
        cls.quantity = fields.DecimalField()

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Discount(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.amount = fields.DecimalField()
        cls.percentage = fields.DecimalField()
        cls.price_book_id = fields.StringField()
        cls.type = fields.EnumField(values=['percentage','fixed_price','amount','free','price_book_price','bonus','total_fixed_price','bonus_choice','bonus_choice'])

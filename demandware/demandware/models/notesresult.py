from decimal import Decimal

from core.model import Model, fields
from demandware.models import note

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class NotesResult(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.notes = fields.ListField(type=note.Note, default_value=[])

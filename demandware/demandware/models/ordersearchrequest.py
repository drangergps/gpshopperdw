from core.model import Model, fields
from decimal import Decimal
import string
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class OrderSearchRequest(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.count = fields.IntField()
        cls.expand = fields.ListField(type=string.String, default_value=[])
        cls.query = fields.EnumField(values=['TermQuery','TextQuery','BoolQuery','MatchAllQuery','MatchAllQuery'])
        cls.select = fields.StringField()
        cls.start = fields.IntField()

from decimal import Decimal

from demandware.models import discount
from demandware.models import promotionlink

from core.model import Model, fields
from demandware.models import shippingmethod

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ApproachingDiscount(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.condition_threshold = fields.DecimalField()
        cls.discount = fields.ModelField(type=discount.Discount, default_value=discount.Discount())
        cls.merchandise_total = fields.DecimalField()
        cls.promotion_link = fields.ModelField(type=promotionlink.PromotionLink, default_value=promotionlink.PromotionLink())
        cls.shipment_id = fields.StringField()
        cls.shipping_methods = fields.ListField(type=shippingmethod.ShippingMethod, default_value=[])
        cls.type = fields.StringField()

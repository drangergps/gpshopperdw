from decimal import Decimal

from core.model import Model, fields
from demandware.models import campaign
from demandware.models import schedule
from demandware.models import slotconfiguration

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SlotConfigurationCampaignAssignment(Model):
    @classmethod
    def _build(cls):
        # Helps get around with circular imports in the module level
        cls.campaign = fields.ModelField(type=campaign.Campaign)
        cls.campaign_id = fields.StringField()
        cls.context = fields.EnumField(values=['global', 'category', 'folder'])
        cls.customer_groups = fields.ListField(type=String)
        cls.description = fields.StringField()
        cls.enabled = fields.BoolField()
        cls.link = fields.StringField()
        cls.rank = fields.IntField()
        cls.schedule = fields.ModelField(type=schedule.Schedule)
        cls.slot_configuration = fields.ModelField(type=slotconfiguration.SlotConfiguration)
        cls.slot_configuration_id = fields.StringField()
        cls.slot_configuration_uuid = fields.StringField()
        cls.slot_context_id = fields.StringField()
        cls.slot_id = fields.StringField()


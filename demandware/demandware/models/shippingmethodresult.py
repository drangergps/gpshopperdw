from decimal import Decimal

from core.model import Model, fields
from demandware.models import shippingmethod

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ShippingMethodResult(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.applicable_shipping_methods = fields.ListField(type=shippingmethod.ShippingMethod, default_value=[])
        cls.default_shipping_method_id = fields.StringField()

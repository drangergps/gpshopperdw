from demandware.models.approachingdiscount import ApproachingDiscount
from demandware.models.approachingdiscountresult import ApproachingDiscountResult
from demandware.models.authrequest import AuthRequest
from demandware.models.basket import Basket
from demandware.models.basketcreaterequest import BasketCreateRequest
from demandware.models.basketpaymentinstrumentrequest import BasketPaymentInstrumentRequest
from demandware.models.basketreference import BasketReference
from demandware.models.basketsresult import BasketsResult
from demandware.models.bonusdiscountlineitem import BonusDiscountLineItem
from demandware.models.bundledproduct import BundledProduct
from demandware.models.bundledproductitem import BundledProductItem
from demandware.models.category import Category
from demandware.models.categoryresult import CategoryResult
from demandware.models.content import Content
from demandware.models.contentfolder import ContentFolder
from demandware.models.contentfolderresult import ContentFolderResult
from demandware.models.contentresult import ContentResult
from demandware.models.contentsearchrefinement import ContentSearchRefinement
from demandware.models.contentsearchrefinementvalue import ContentSearchRefinementValue
from demandware.models.contentsearchresult import ContentSearchResult
from demandware.models.couponitem import CouponItem
from demandware.models.customobject import CustomObject
from demandware.models.customer import Customer
from demandware.models.customeraddress import CustomerAddress
from demandware.models.customeraddresslink import CustomerAddressLink
from demandware.models.customeraddressresult import CustomerAddressResult
from demandware.models.customerinfo import CustomerInfo
from demandware.models.customerorderresult import CustomerOrderResult
from demandware.models.customerpaymentcardrequest import CustomerPaymentCardRequest
from demandware.models.customerpaymentinstrument import CustomerPaymentInstrument
from demandware.models.customerpaymentinstrumentrequest import CustomerPaymentInstrumentRequest
from demandware.models.customerpaymentinstrumentresult import CustomerPaymentInstrumentResult
from demandware.models.customerproductlist import CustomerProductList
from demandware.models.customerproductlistitem import CustomerProductListItem
from demandware.models.customerproductlistitemlink import CustomerProductListItemLink
from demandware.models.customerproductlistitempurchase import CustomerProductListItemPurchase
from demandware.models.customerproductlistitempurchaseresult import CustomerProductListItemPurchaseResult
from demandware.models.customerproductlistitemresult import CustomerProductListItemResult
from demandware.models.customerproductlistregistrant import CustomerProductListRegistrant
from demandware.models.customerproductlistresult import CustomerProductListResult
from demandware.models.customerregistration import CustomerRegistration
from demandware.models.discount import Discount
from demandware.models.discountrequest import DiscountRequest
from demandware.models.fault import Fault
from demandware.models.flash import Flash
from demandware.models.giftcertificateitem import GiftCertificateItem
from demandware.models.giftcertificaterequest import GiftCertificateRequest
from demandware.models.image import Image
from demandware.models.imagegroup import ImageGroup
from demandware.models.inventory import Inventory
from demandware.models.itemkey import ItemKey
from demandware.models.locale import  Locale
from demandware.models.markuptext import MarkupText
from demandware.models.master import Master
from demandware.models.mediafile import MediaFile
from demandware.models.nocontent import NoContent
from demandware.models.note import Note
from demandware.models.notesresult import NotesResult
from demandware.models.option import Option
from demandware.models.optionitem import OptionItem
from demandware.models.optionvalue import OptionValue
from demandware.models.order import Order
from demandware.models.orderaddress import OrderAddress
from demandware.models.orderpaymentcardrequest import OrderPaymentCardRequest
from demandware.models.orderpaymentinstrument import OrderPaymentInstrument
from demandware.models.orderpaymentinstrumentrequest import OrderPaymentInstrumentRequest
from demandware.models.ordersearchhit import OrderSearchHit
from demandware.models.ordersearchrequest import OrderSearchRequest
from demandware.models.ordersearchresult import OrderSearchResult
from demandware.models.passwordchangerequest import PasswordChangeRequest
from demandware.models.passwordreset import PasswordReset
from demandware.models.paymentbankaccount import PaymentBankAccount
from demandware.models.paymentbankaccountrequest import PaymentBankAccountRequest
from demandware.models.paymentcard import PaymentCard
from demandware.models.paymentcardrequest import PaymentCardRequest
from demandware.models.paymentcardspec import PaymentCardSpec
from demandware.models.paymentmethod import PaymentMethod
from demandware.models.paymentmethodresult import PaymentMethodResult
from demandware.models.priceadjustment import PriceAdjustment
from demandware.models.priceadjustmentlimit import PriceAdjustmentLimit
from demandware.models.priceadjustmentlimits import PriceAdjustmentLimits
from demandware.models.priceadjustmentrequest import PriceAdjustmentRequest
from demandware.models.product import Product
from demandware.models.productdetailslink import ProductDetailsLink
from demandware.models.productitem import ProductItem
from demandware.models.productlink import ProductLink
from demandware.models.productlistevent import ProductListEvent
from demandware.models.productlistitem import ProductListItem
from demandware.models.productlistitemreference import ProductListItemReference
from demandware.models.productlistlink import ProductListLink
from demandware.models.productlistregistrant import ProductListRegistrant
from demandware.models.productlistshippingaddress import ProductListShippingAddress
from demandware.models.productpromotion import ProductPromotion
from demandware.models.productref import ProductRef
from demandware.models.productresult import ProductResult
from demandware.models.productsearchhit import ProductSearchHit
from demandware.models.productsearchrefinement import ProductSearchRefinement
from demandware.models.productsearchrefinementvalue import ProductSearchRefinementValue
from demandware.models.productsearchresult import ProductSearchResult
from demandware.models.productsearchsortingoption import ProductSearchSortingOption
from demandware.models.productsimplelink import ProductSimpleLink
from demandware.models.producttype import ProductType
from demandware.models.promotion import Promotion
from demandware.models.promotionlink import PromotionLink
from demandware.models.promotionresult import PromotionResult
from demandware.models.publicproductlist import PublicProductList
from demandware.models.publicproductlistitem import PublicProductListItem
from demandware.models.publicproductlistitemresult import PublicProductListItemResult
from demandware.models.publicproductlistlink import PublicProductListLink
from demandware.models.publicproductlistresult import PublicProductListResult
from demandware.models.recommendation import Recommendation
from demandware.models.recommendationtype import RecommendationType
from demandware.models.shipment import Shipment
from demandware.models.shippingitem import ShippingItem
from demandware.models.shippingmethod import ShippingMethod
from demandware.models.shippingmethodresult import ShippingMethodResult
from demandware.models.shippingpromotion import ShippingPromotion
from demandware.models.simplelink import SimpleLink
from demandware.models.site import Site
from demandware.models.slotconfiguration import SlotConfiguration
from demandware.models.slot import Slot
from demandware.models.slotcontent import SlotContent
from demandware.models.slotconfigurationcampaignassignnment import SlotConfigurationCampaignAssignment
from demandware.models.slotconfigurationabtestgroupassignment import SlotConfigurationABTestGroupAssignment
from demandware.models.slotconfigurationassignmentinformation import SlotConfigurationAssignmentInformation
from demandware.models.schedule import Schedule
from demandware.models.campaign import Campaign
from demandware.models.markuptext import MarkupText
from demandware.models.recurrence import Recurrence
from demandware.models.slots import Slots
from demandware.models.timeofday import TimeOfDay
from demandware.models.sortfield import SortField
from demandware.models.status import  Status
from demandware.models.store import Store
from demandware.models.storeresult import StoreResult
from demandware.models.suggestedcategory import SuggestedCategory
from demandware.models.suggestedcontent import SuggestedContent
from demandware.models.suggestedphrase import SuggestedPhrase
from demandware.models.suggestedproduct import SuggestedProduct
from demandware.models.suggestedterm import SuggestedTerm
from demandware.models.suggestedterms import SuggestedTerms
from demandware.models.suggestion import Suggestion
from demandware.models.suggestionresult import SuggestionResult
from demandware.models.variant import Variant
from demandware.models.variationattribute import VariationAttribute
from demandware.models.variationattributevalue import VariationAttributeValue
from demandware.models.variationgroup import VariationGroup

from core.model import Model
from demandware.models.suggestedcontent import SuggestedContent

for key, value in globals().copy().items():

       try:
            inner_value = value.__dict__.items()
       except AttributeError:
            inner_value = {}

       for ref_key, ref_value in inner_value:

           try:
                if issubclass(ref_value, Model) and ref_value != Model:
                    ref_value._build()
           except (TypeError, AttributeError):
                pass
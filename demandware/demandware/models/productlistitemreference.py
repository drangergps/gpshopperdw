from decimal import Decimal

from core.model import Model, fields
from demandware.models import productlistlink

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductListItemReference(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.id = fields.StringField()
        cls.product_list = fields.ModelField(type=productlistlink.ProductListLink, default_value=productlistlink.ProductListLink())

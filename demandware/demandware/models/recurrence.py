from decimal import Decimal

from core.model import Model, fields
from demandware.models import timeofday

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Recurrence(Model):
    @classmethod
    def _build(cls):
        # Helps get around with circular imports in the module level
        cls.day_of_week = fields.StringField()
        cls.time_of_day = fields.ModelField(type=timeofday.TimeOfDay)

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SuggestedTerm(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.completed = fields.BoolField()
        cls.corrected = fields.BoolField()
        cls.exact_match = fields.BoolField()
        cls.value = fields.StringField()

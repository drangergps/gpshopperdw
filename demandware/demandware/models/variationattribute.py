from decimal import Decimal

from core.model import Model, fields
from demandware.models import variationattributevalue

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class VariationAttribute(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.id = fields.StringField()
        cls.name = fields.StringField()
        cls.values = fields.ListField(type=variationattributevalue.VariationAttributeValue, default_value=[])

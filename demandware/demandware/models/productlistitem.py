from decimal import Decimal

from core.model import Model, fields
from demandware.models import productdetailslink

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductListItem(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.id = fields.StringField()
        cls.priority = fields.IntField()
        cls.product_details_link = fields.ModelField(type=productdetailslink.ProductDetailsLink, default_value=productdetailslink.ProductDetailsLink())
        cls.public = fields.BoolField()
        cls.purchased_quantity = fields.DecimalField()
        cls.quantity = fields.DecimalField()
        cls.type = fields.EnumField(values=['product','product'])

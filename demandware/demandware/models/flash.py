from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Flash(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.details = fields.DictField()
        cls.message = fields.StringField()
        cls.path = fields.StringField()
        cls.type = fields.StringField()

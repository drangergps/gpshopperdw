from decimal import Decimal

from core.model import Model, fields
from demandware.models import recurrence

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Schedule(Model):
    @classmethod
    def _build(cls):
        # Helps get around with circular imports in the module level
        cls.end_date = fields.StringField()
        cls.recurrence = fields.ModelField(type=recurrence.Recurrence)
        cls.start_date = fields.StringField()

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Store(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        from demandware.models import mediafile
        from demandware.models import markuptext

        cls.address1 = fields.StringField()
        cls.address2 = fields.StringField()
        cls.city = fields.StringField()
        cls.country_code = fields.StringField()
        cls.distance = fields.DecimalField()
        cls.distance_unit = fields.StringField()
        cls.email = fields.StringField()
        cls.fax = fields.StringField()
        cls.id = fields.StringField()
        cls.image = fields.ModelField(type=mediafile.MediaFile, default_value=mediafile.MediaFile())
        cls.inventory_id = fields.StringField()
        cls.latitude = fields.DecimalField()
        cls.longitude = fields.DecimalField()
        cls.name = fields.StringField()
        cls.phone = fields.StringField()
        cls.pos_enabled = fields.BoolField()
        cls.postal_code = fields.StringField()
        cls.state_code = fields.StringField()
        cls.store_events = fields.ModelField(type=markuptext.MarkupText, default_value=markuptext.MarkupText())
        cls.store_hours = fields.ModelField(type=markuptext.MarkupText, default_value=markuptext.MarkupText())
        cls.store_locator_enabled = fields.BoolField()
        cls.last_modified = fields.StringField(required=False)
        cls.creation_date = fields.StringField(required=False)


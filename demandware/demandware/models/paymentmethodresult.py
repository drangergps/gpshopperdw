from decimal import Decimal

from core.model import Model, fields
from demandware.models import paymentmethod

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PaymentMethodResult(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.applicable_payment_methods = fields.ListField(type=paymentmethod.PaymentMethod, default_value=[])

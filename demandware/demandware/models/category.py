from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Category(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.description = fields.StringField()
        cls.id = fields.StringField()
        cls.image = fields.StringField()
        cls.name = fields.StringField()
        cls.page_description = fields.StringField()
        cls.page_keywords = fields.StringField()
        cls.page_title = fields.StringField()
        cls.parent_category_id = fields.StringField()
        cls.thumbnail = fields.StringField()
        cls.last_modified = fields.StringField(required=False)
        cls.creation_date = fields.StringField(required=False)
Category.categories = fields.ListField(type=Category, default_value=[])
from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Promotion(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.callout_msg = fields.StringField()
        cls.currency = fields.StringField()
        cls.details = fields.StringField()
        cls.discounted_products_link = fields.StringField()
        cls.end_date = fields.StringField()
        cls.id = fields.StringField()
        cls.image = fields.StringField()
        cls.name = fields.StringField()
        cls.start_date = fields.StringField()
        cls.last_modified = fields.StringField(required=False)
        cls.creation_date = fields.StringField(required=False)

from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class OrderAddress(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.address1 = fields.StringField()
        cls.address2 = fields.StringField()
        cls.city = fields.StringField()
        cls.company_name = fields.StringField()
        cls.country_code = fields.StringField()
        cls.first_name = fields.StringField()
        cls.full_name = fields.StringField()
        cls.id = fields.StringField()
        cls.job_title = fields.StringField()
        cls.last_name = fields.StringField()
        cls.phone = fields.StringField()
        cls.post_box = fields.StringField()
        cls.postal_code = fields.StringField()
        cls.salutation = fields.StringField()
        cls.second_name = fields.StringField()
        cls.state_code = fields.StringField()
        cls.suffix = fields.StringField()
        cls.suite = fields.StringField()
        cls.title = fields.StringField()

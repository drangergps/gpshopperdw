from core.model import Model, fields
from decimal import Decimal
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Locale(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.country = fields.StringField()
        cls.default = fields.BoolField()
        cls.display_country = fields.StringField()
        cls.display_language = fields.StringField()
        cls.display_name = fields.StringField()
        cls.id = fields.StringField()
        cls.iso3_country = fields.StringField()
        cls.iso3_language = fields.StringField()
        cls.language = fields.StringField()
        cls.name = fields.StringField()

from decimal import Decimal

from core.model import Model, fields
from demandware.models import customeraddress
from demandware.models import customerpaymentinstrument

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Customer(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.addresses = fields.ListField(type=customeraddress.CustomerAddress, default_value=[])
        cls.auth_type = fields.EnumField(values=['guest','registered'], default_value="guest")
        cls.birthday = fields.StringField()
        cls.company_name = fields.StringField()
        cls.creation_date = fields.StringField(required=False)
        cls.customer_id = fields.StringField()
        cls.customer_no = fields.StringField()
        cls.email = fields.StringField()
        cls.enabled = fields.BoolField()
        cls.fax = fields.StringField()
        cls.first_name = fields.StringField()
        cls.gender = fields.IntField()
        cls.job_title = fields.StringField()
        cls.last_login_time = fields.StringField()
        cls.last_modified = fields.StringField(required=False)
        cls.last_name = fields.StringField()
        cls.last_visit_time = fields.StringField()
        cls.login = fields.StringField()
        cls.note = fields.StringField()
        cls.payment_instruments = fields.ListField(type=customerpaymentinstrument.CustomerPaymentInstrument, default_value=[])
        cls.phone_business = fields.StringField()
        cls.phone_home = fields.StringField()
        cls.phone_mobile = fields.StringField()
        cls.preferred_locale = fields.StringField()
        cls.previous_login_time = fields.StringField()
        cls.previous_visit_time = fields.StringField()
        cls.salutation = fields.StringField()
        cls.second_name = fields.StringField()
        cls.suffix = fields.StringField()
        cls.title = fields.StringField()

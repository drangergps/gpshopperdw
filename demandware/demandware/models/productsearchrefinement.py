from decimal import Decimal

from core.model import Model, fields
from demandware.models import productsearchrefinementvalue

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ProductSearchRefinement(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.attribute_id = fields.StringField()
        cls.label = fields.StringField()
        cls.values = fields.ListField(type=productsearchrefinementvalue.ProductSearchRefinementValue, default_value=[])

from decimal import Decimal

from core.model import Model, fields

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class ContentFolderResult(Model):
    @classmethod
    def _build(cls):
        from demandware.models import contentfolder
    # Helps get around with circular imports in the module level
        cls.count = fields.IntField()
        cls.data = fields.ListField(type=contentfolder.ContentFolder, default_value=[])
        cls.total = fields.IntField()

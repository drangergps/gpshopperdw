from decimal import Decimal

from core.model import Model, fields
from demandware.models import image
from demandware.models import recommendationtype

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Recommendation(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.callout_msg = fields.StringField()
        cls.image = fields.ModelField(type=image.Image, default_value=image.Image())
        cls.long_description = fields.StringField()
        cls.name = fields.StringField()
        cls.recommendation_type = fields.ModelField(type=recommendationtype.RecommendationType, default_value=recommendationtype.RecommendationType())
        cls.recommended_item_id = fields.StringField()
        cls.recommended_item_link = fields.StringField()
        cls.short_description = fields.StringField()

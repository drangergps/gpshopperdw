from decimal import Decimal

from core.model import Model, fields
from demandware.models import markuptext

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SlotContent(Model):
    @classmethod
    def _build(cls):
        # Helps get around with circular imports in the module level
        cls.body = fields.ModelField(type=markuptext.MarkupText)
        cls.category_ids = fields.ListField(type=String)
        cls.content_asset_ids = fields.ListField(type=String)
        cls.product_ids = fields.ListField(type=String)
        cls.type = fields.EnumField(values=['products', 'categories', 'content_assets', 'html', 'recommended_products'])
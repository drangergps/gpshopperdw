from core.model import Model, fields
from decimal import Decimal
import string
import string
# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class PaymentCardSpec(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.card_type = fields.StringField()
        cls.checksum_verification_enabled = fields.BoolField()
        cls.description = fields.StringField()
        cls.image = fields.StringField()
        cls.name = fields.StringField()
        cls.number_lengths = fields.ListField(type=string.String, default_value=[])
        cls.number_prefixes = fields.ListField(type=string.String, default_value=[])
        cls.security_code_length = fields.IntField()

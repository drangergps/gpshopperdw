from decimal import Decimal

from core.model import Model, fields
from demandware.models import suggestedterm

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class SuggestedTerms(Model):
    @classmethod
    def _build(cls):
    # Helps get around with circular imports in the module level
        cls.original_term = fields.StringField()
        cls.terms = fields.ListField(type=suggestedterm.SuggestedTerm, default_value=[])

from decimal import Decimal

from core.model import Model, fields

# AUTO GENERATED. MAKE CHANGES WITH CAUTION AS THEY MAYBE OVERWRITTEN.

String = str
Boolean = bool
Date = str
Dict = dict
Money = Decimal

class Product(Model):
    @classmethod
    def _build(cls):

        from demandware.models import bundledproduct
        from demandware.models import imagegroup
        from demandware.models import inventory
        from demandware.models import master
        from demandware.models import option
        from demandware.models import productlink
        from demandware.models import productpromotion
        from demandware.models import recommendation
        from demandware.models import producttype
        from demandware.models import variant
        from demandware.models import variationattribute
        from demandware.models import variationgroup

        cls.brand = fields.StringField()
        cls.bundled_products = fields.ListField(type=bundledproduct.BundledProduct, default_value=[])
        cls.currency = fields.StringField()
        cls.ean = fields.StringField()
        cls.id = fields.StringField()
        cls.image_groups = fields.ListField(type=imagegroup.ImageGroup, default_value=[])
        cls.inventories = fields.ListField(type=inventory.Inventory, default_value=[])
        cls.inventory = fields.ModelField(type=inventory.Inventory, default_value=inventory.Inventory())
        cls.long_description = fields.StringField()
        cls.manufacturer_name = fields.StringField()
        cls.manufacturer_sku = fields.StringField()
        cls.master = fields.ModelField(type=master.Master, default_value=master.Master())
        cls.min_order_quantity = fields.DecimalField()
        cls.name = fields.StringField()
        cls.options = fields.ListField(type=option.Option, default_value=[])
        cls.page_description = fields.StringField()
        cls.page_keywords = fields.StringField()
        cls.page_title = fields.StringField()
        cls.price = fields.DecimalField()
        cls.price_max = fields.DecimalField()
        cls.prices = fields.DictField()
        cls.primary_category_id = fields.StringField()
        cls.product_links = fields.ListField(type=productlink.ProductLink, default_value=[])
        cls.product_promotions = fields.ListField(type=productpromotion.ProductPromotion, default_value=[])
        cls.recommendations = fields.ListField(type=recommendation.Recommendation, default_value=[])
        cls.short_description = fields.StringField()
        cls.step_quantity = fields.DecimalField()
        cls.type = fields.ModelField(type=producttype.ProductType, default_value=producttype.ProductType())
        cls.unit = fields.StringField()
        cls.upc = fields.StringField()
        cls.variants = fields.ListField(type=variant.Variant, default_value=[])
        cls.variation_attributes = fields.ListField(type=variationattribute.VariationAttribute, default_value=[])
        cls.variation_groups = fields.ListField(type=variationgroup.VariationGroup, default_value=[])
        cls.variation_values = fields.DictField()

Product.set_products = fields.ListField(type=Product, default_value=[])
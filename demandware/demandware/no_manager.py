import pkgutil
import os
import importlib

class NoManager(object):
    def __init__(self, base_url, client_id, locale=None, currency=None):
        self.base_url = base_url
        self.client_id = client_id
        for module in [name for _, name, _ in pkgutil.iter_modules([os.path.dirname(os.path.realpath(__file__))+'/resources'])]:
            setattr(self, module, importlib.import_module('demandware.resources.'+module, __name__))
            try:
                setattr(self, module, getattr(self,module).Api(base_url=base_url, client_id=client_id, locale=locale, currency=currency))
            except:
                #No Api subclass? No problem
                pass


from demandware.business import Demandware

class Store(Demandware):
    def fetch_all_stores_by_zip(self, postal_code, country_code="US", max_distance="20.00", distance_unit="mi", **kwargs):

        return self._return_handler(self.resources.stores.all_get(postal_code=postal_code, country_code=country_code, max_distance=max_distance, distance_unit=distance_unit, **kwargs))

    def fetch_store_by_id(self, store_id, **kwargs):

        return self._return_handler(self.resources.stores.get(id=store_id, **kwargs))

    def fetch_stores_by_ids(self, store_ids):

        return self._return_handler(self.resources.stores.get_many(ids=store_ids))

    def fetch_all_stores_by_latlon(self, latitude, longitude, distance_unit=None, country_code="US", **kwargs):

        return self._return_handler(self.resources.stores.all_get(latitude=latitude, longitude=longitude, country_code=country_code, distance_unit=distance_unit, **kwargs))

    def fetch_stores_by_latlon(self, latitude, longitude, country_code="US", max_distance="20.0", distance_unit="mi", **kwargs):

        return self._return_handler(self.resources.stores.all_get(latitude=latitude, longitude=longitude, country_code=country_code, max_distance=max_distance, distance_unit=distance_unit))

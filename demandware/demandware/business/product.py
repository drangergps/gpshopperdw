from demandware.business import Demandware

class Product(Demandware):
    def fetch_product(self, product_id, translator=None):
        """
        Fetches a product based on product_id

        :type product_id: str
        :rtype: demandware.xapi.models.product.Product
        """

        return self._return_handler(self.resources.products.get(product_id), translator)

    def fetch_many_product_details(self, product_ids, translator=None):
        return self._return_handler(self.resources.products.get_many(product_ids), translator)

    def fetch_product_details(self, product_id):
        return self.resources.products.get(id=product_id)

    def fetch_product_recommendations(self, product_id):
        return self.resources.products.recommendations_get(id=product_id, input={})
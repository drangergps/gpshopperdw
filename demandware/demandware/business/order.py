from demandware.business import Demandware

class Order(Demandware):

    def fetch_order(self, order_number):

        return self._return_handler(self.resources.orders.get(order_no=order_number))

    def update_payment(self, order_number, payment_id, input):
        return self._return_handler(self.resources.orders.payment_instruments_patch(order_no=order_number, payment_instrument_id=payment_id, input=input))

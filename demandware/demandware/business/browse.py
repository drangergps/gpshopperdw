from demandware.business import Demandware
from collections import deque
import itertools
try:
    from urllib import quote
except ImportError:
    from urllib.parse import quote

class Browse(Demandware):
    def fetch_categories(self, browse_level=None):
        browse_cat = 'root'  # brands-adidas-mens
        if browse_level == 1:
            return self.resources.categories.get(id=browse_cat)
        if browse_level == 2:
            return (self.resources.categories.get_many([browse_cat], levels=2)).data[0]
        if browse_level is None:
            levels_deep = 4
        else:
            levels_deep = browse_level
        result = self.resources.categories.get_many(ids=[browse_cat], levels=2)

        dict_cat_tree = {result.data[0]['id']: result.data[0]}
        queue = deque()
        for category in result.data[0].categories:
            queue.append((category, 2))

        def category_traversal(queue):  # breadth first tree traversal
            while queue:
                nodes = queue.popleft()
                list_cat = []
                list_from_get = []
                if nodes[0].categories:
                    if (nodes[1] + 1) < levels_deep:
                        level = 2
                    else:
                        level = 1
                    for category in nodes[0].categories:
                        list_cat.append(quote(category["id"]))
                    for sublist in range(0, len(list_cat), 50):
                        res_get_cat = self.resources.categories.get_many(list_cat[sublist:sublist + 50], levels=level)
                        list_from_get.append(res_get_cat.data)
                    list_from_get = list(itertools.chain(*list_from_get))
                    counter = nodes[1] + level
                    update_tree(list_from_get)
                    update_queue(list_from_get, queue, counter)

        def update_tree(list_from_get):
            for category in list_from_get:
                update_dict(dict_cat_tree.get(browse_cat), category)

        def update_queue(list_from_get, queue, counter):
            if counter < levels_deep:
                for category in list_from_get:
                    for level_in_cat in category.categories:
                        queue.append((level_in_cat, counter))
            return queue

        def update_dict(dict_to_update, cat):
            for category in dict_to_update.categories:
                if cat.parent_category_id == dict_to_update.id:
                    if category.id == cat.id:
                        category.categories = cat.categories
                        break
                else:
                    update_dict(category, cat)
            return dict_to_update

        category_traversal(queue)
        return result.data[0]

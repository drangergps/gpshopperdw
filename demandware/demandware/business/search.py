from demandware.business import Demandware


class Search(Demandware):
    def search(self, input, translator=None):
        """
        :type input: SearchQuery | dict
        :rtype: demandware.models.productsearchresult.ProductSearchResult
        """

        try:
            input.validate()
        except AttributeError:
            pass

        return self._return_handler(self.resources.product_search.get(**dict(input)), translator)

    def search_suggestions(self, input, translator=None):
        """
        :type input: SuggestiveSearchQuery | dict
        :rtype: demandware.models.suggestionresult.SuggestionResult
        """
        return self._return_handler(self.resources.search_suggestions.get(**dict(input)), translator)

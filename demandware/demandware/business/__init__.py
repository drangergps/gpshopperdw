import logging
global_logger = logging.getLogger(__name__)

from core.model import Model, fields
from demandware.jwt_manager import JwtManager, JwtToken
from demandware.oauth_manager import OauthManager
from demandware.no_manager import NoManager

class SearchQuery(Model):

    def __init__(self, *args, **kwargs):
        super(SearchQuery, self).__init__(*args, **kwargs)
        self._refine_buffer = []

    def add_refine_param(self, param):
        for key, value in param.items():
            if type(value) in (list, tuple):
                value = "|".join([str(item) for item in value])
            self._refine_buffer.append(key + "=" + value)

    def add_refine_range(self, key, range):

        if not range:
            return

        if len(range) <= 1:
            self.add_refine_param({key: range})
            return

        self._refine_buffer.append(key + "=(" + str(range[0]) + ".." + str(range[-1]) + ")")

    @property
    def refine(self):
        return self._refine_buffer

    count = fields.IntField(default_value=100)
    currency = fields.StringField()
    expand = fields.StringField(default_value="availability, images, prices, variations")
    locale = fields.StringField()
    q = fields.StringField(required=True)
    sort = fields.StringField()
    start = fields.IntField(default_value=0)

class AuthorizationSchemes:
    JWT = JwtManager
    OAUTH = OauthManager
    NONE = NoManager

    @classmethod
    def enumerate_values(cls):
        return [
            getattr(cls, var)
            for var in vars(cls).keys()
            if var not in ["__doc__", "__module__", "enumerate_values"]
        ]

class Demandware(object):
    """
    Bussiness logic layer for the demandware implementation.
    This file is expected to grow as we add more bussiness logic. For now, may method may appear
    to be a pass through.

    Added oauth requirements, which would require initializations form mobile's engine TODO: Read more about this
    """

    resource_managers = {
        AuthorizationSchemes.JWT: None,
        AuthorizationSchemes.OAUTH: None,
        AuthorizationSchemes.NONE: None,
    }

    #Rather than have this function grow in parameters every time we add an authorization method, let's pass the authorization method through.
    #This requires extra work in the calling code, so we raise an assertion if the correct configurations are not passed.
    #This simplifies the interface here and makes it extensible, utilizing the Introduct Parameter Object pattern:
    #https://sourcemaking.com/refactoring/introduce-parameter-object
    def __init__(self, managers, default_authorization_scheme = AuthorizationSchemes.JWT):

        #We need at least one authorization scheme
        try:
            assert len(managers)
        except:
            raise AssertionError("Please pass a list of at least one manager to initialize this class")
        #We need a VALID authorization scheme
        try:
            assert default_authorization_scheme in (AuthorizationSchemes.enumerate_values())
        except:
            raise AssertionError("Please pass a valid authorization scheme to initialize this class")

        for manager in managers:
            self.resource_managers[type(manager)] = manager

        self.resources = self.resource_managers[default_authorization_scheme]

        if hasattr(self.resources, "session") and not (getattr(self.resources.session, "demandware", None)):
            self.resources.session.demandware = dict()

    #This will either be: 1 engine with N auth schemes (so, self.resources[JWT_CONST], self.resources[OAUTH_CONST] etc.
    #Or N engines with 1 auth scheme (so each has self.resources with the appropriate auth scheme
    #(prefer second option?? Prevents users from accidentally calling an auth scheme that is uninitialized)
    #(prefer first option?? All our code is written using self.engine, assuming there is one engine per client. Changing this pattern requires massive rework
    #Plus an engine is not equal to an authentication scheme
    #The below solution in def __call__ uses a little of both options. There is one engine, but calling that engine returns an instance of the engine with the new auth scheme
    #This basically makes JWT a default
    def use_auth_scheme(self, authorization_scheme=AuthorizationSchemes.JWT):
        #Temporary? Say we initialize the class with an authorization scheme...
        #This will return another instance of the class with potentially a different scheme
        #But will hide this fact and act like one engine
        #I think this is better than mutating the class's actual state because of potential confusion if you forget to set it back
        #This has the added benefit of covering the above case because if you want to reset the scheme you can just say engine = engine()

        #Wish we could just make this a class method... unfortunately it will be called from an instance
        demandware_class = self.__class__

        return demandware_class(
            managers = [
                manager for manager in self.resource_managers.values()
            ],
            default_authorization_scheme=authorization_scheme,
        )

    @property
    def basket_id(self):
        try:
            #Are we aready using a basket?
            basket_id = self.resources.session.demandware["basket_id"]
            if basket_id:
                return basket_id
        except (AttributeError,KeyError,TypeError):
            pass # No basket in the session, try and get one.

        baskets = self.resources.customers.baskets_get(customer_id=self.customer_id).baskets
        # We default to the 0th basket because JWT auth in demandware does not permit multiple baskets.
        if len(baskets) > 0:
            self.basket_id = baskets[0].basket_id
            return self.basket_id

        #If we aren't already using a basket and we didn't get one, let's create one

        self.basket_id = self.resources.baskets.post(input={'currency': self.resources.currency}).basket_id

        # If we get a demandware fault on either of these calls, we're pretty toast, so we're not going to catch it.
        return self.basket_id

    @property
    def customer_no(self):
        try:
            customer_no = self.resources.session.demandware["customer_no"]
            if customer_no:
                return customer_no
        except (AttributeError,KeyError,TypeError):
            pass

        try:
            customer = self.resources.customers.get(customer_id=self.customer_id)
        #If this call fails the customer doesn't exist or isn't logged in
        except:
            return None

        return customer.customer_no

    @customer_no.setter
    def customer_no(self, customer_no):
        self.resources.session.demandware = getattr(self.resources.session, "demandware", None) or dict()
        self.resources.session.demandware["customer_no"] = customer_no

    @basket_id.setter
    def basket_id(self, new_basket_id):
        self.resources.session.demandware = getattr(self.resources.session, "demandware", None) or dict()
        self.resources.session.demandware["basket_id"] = new_basket_id

    @property
    def customer_product_list_id(self):
        try: # Is there a customer product list in the session?
            customer_product_list_id = self.resources.session.demandware["customer_product_list_id"]
            if customer_product_list_id:
                return customer_product_list_id
        except (AttributeError,KeyError,TypeError):
            pass # No customer product list in the session, try and get one

        customer_product_lists = self.resources.customers.product_lists_all_get()
        if customer_product_lists.count > 0:
            # The typical setup only uses one list.
            self.customer_product_list_id = customer_product_lists.data[0].id
        else: # Customer has no product list, create one and save the id
            self.customer_product_list_id = self.resources.customers.product_lists_post(None, {"type": "wish_list"}).id # Let the JwtManager add the customer_id

        return self.customer_product_list_id

    @customer_product_list_id.setter
    def customer_product_list_id(self, new_customer_product_list_id):
        self.resources.session.demandware = getattr(self.resources.session, "demandware", None) or dict()
        self.resources.session.demandware["customer_product_list_id"] = new_customer_product_list_id

    @property
    def customer_id(self):
        try:
            # Since self.resources can either be authenticated via jwt or oauth, we might not always have token set(token exists only when authentication happens via jwt).
            # Hence we will reach out to the demandware session to fetch the same.
            token = JwtToken(self.resources.session.demandware["jwt_token"])
            if hasattr(token, "customer_id"):
                return token.customer_id
        except (AttributeError, KeyError, TypeError):
            pass

        return None

    @customer_id.setter
    def customer_id(self, customer_id):
        self.resources.session.demandware = getattr(self.resources.session, "demandware", None) or dict()
        self.resources.session.demandware["customer_id"] = customer_id


    @staticmethod
    def _return_handler(result, translator=None):
        """
        Handles the translator if one is supplied
        """
        if translator:
            return translator(result).to_model()
        return result

    def _get_shipment_id(self, basket_id, shipment_id):
        basket = self.fetch_cart(basket_id)
        try:
            shipment_id = basket.shipments[0].shipment_id
        except (IndexError):
            raise ValueError("Unable to determine shipment id")
        return shipment_id


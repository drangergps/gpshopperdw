from demandware.business import Demandware
from demandware import utils


class Cart(Demandware):
    def create_cart(self, input, translator=None):
        """
        Crates a new basket (cart) in demandware

        :rtype: demandware.models.basket.Basket
        """
        cart = self._return_handler(self.resources.baskets.post(dict(input)), translator)
        self.basket_id = cart.basket_id
        return cart

    def add_basket_note(self, input, basket_id=None, translator=None):
        """
        Adds a note to an existing basket
        :param basket_id: str
        :param input: subject and text
        :rtype: demandware.models.basket.Basket
        """
        return self._return_handler(self.resources.baskets.notes_post(basket_id=(basket_id), input=dict(input)), translator)

    def get_basket_notes(self, basket_id=None, translator=None):
        """
        Adds a note to an existing basket
        :param basket_id: str
        :rtype: demandware.models.notesresult.NotesResult
        """
        return self._return_handler(self.resources.baskets.notes_get(basket_id=(basket_id)), translator)

    def cart_add_product(self, product_item, basket_id=None, translator=None):
        """

        :type basket_id: str
        :type product_item: demandware.models.productitem.ProductItem
        :rtype: demandware.models.basket.Basket
        """

        return self._return_handler(self.resources.baskets.items_post(basket_id=(basket_id), input=[dict(product_item)]), translator)

    def cart_add_products(self, product_items, basket_id=None, translator=None):
        """

        :type basket_id: str
        :type product_items: list of demandware.models.productitem.ProductItem
        :rtype: demandware.models.basket.Basket
        """

        return self._return_handler(self.resources.baskets.items_post(basket_id, product_items), translator)

    def cart_remove_product(self, item_id, basket_id=None, translator=None):
        """
        Removes an item from the cart.

        Note: Demandware does not raise an error if the item you are removing does not match the id in the cart

        :type basket_id: str
        :type item_id: str
        :item_id: The item_id of the item in the cart. This is not the same as product_id. ex. cart.product_items[0].item_id
        :rtype: demandware.models.basket.Basket
        """

        return self._return_handler(self.resources.baskets.items_delete(basket_id, item_id), translator)

    def cart_update_product(self, product_item, item_id, basket_id=None, translator=None):
        """
        Removes an item from the cart.

        Note: Demandware does not raise an error if the item you are removing does not match the id in the cart

        :type basket_id: str
        :type item_id: str
        :item_id: The item_id of the item in the cart. This is not the same as product_id. ex. cart.product_items[0].item_id
        :rtype: demandware.models.basket.Basket
        """

        return self._return_handler(self.resources.baskets.items_patch(basket_id, item_id, dict(product_item)), translator)

    def fetch_cart(self, basket_id=None):
        """

        :type basket_id: str
        :rtype: demandware.models.basket.Basket
        """

        return self._return_handler(self.resources.baskets.get(basket_id=basket_id))

    def delete_cart(self, basket_id=None):

        return self._return_handler(self.resources.baskets.delete(basket_id=basket_id))

    def assign_cart(self, basket, translator=None):
        """

        :type basket_id: str
        :rtype: demandware.models.basket.Basket
        """

        #Delete the customer's cart if it exists
        #If it doesn't one gets created when referencing basket_id so this will never throw an error
        baskets = self.resources.customers.baskets_get().baskets
        if baskets:
            self.delete_cart(basket_id=baskets[0].basket_id)
        return self.create_cart(utils.make_basket_submittable(basket), translator)

    def fetch_payment_methods(self, basket_id=None):
        """

        :type basket_id: str
        :rtype: demandware.models.paymentmethodresult.PaymentMethodResult
        """

        return self._return_handler(self.resources.baskets.payment_methods_get(basket_id))

    def set_payment(self, payment, basket_id=None):
        """

        :type basket_id: str
        :type payment: demandware.models.basketpaymentinstrumentrequest.BasketPaymentInstrumentRequest
        :rtype: demandware.models.basket.Basket
        """

        return self._return_handler(self.resources.baskets.payment_instruments_post(basket_id, payment))

    def remove_payment(self, payment_id, basket_id=None):
        """

        :type basket_id: str
        :type payment_id: str
        :rtype: demandware.models.basket.Basket
        """

        return self._return_handler(self.resources.baskets.payment_instruments_delete(basket_id, payment_id))

    def fetch_shipping_methods(self, basket_id=None, shipment_id=None):

        basket_id = basket_id

        if not shipment_id:
            shipment_id = self._get_shipment_id(basket_id, shipment_id)

        return self._return_handler(self.resources.baskets.shipments_shipping_methods_get(basket_id, shipment_id))


    def set_shipping_method(self, shipping_method, basket_id=None, shipment_id=None):
        """

        :type basket_id: str
        :type shipping_method: demandware.models.shippingmethod.ShippingMethod
        :rtype: demandware.models.basket.Basket
        """

        basket_id = basket_id

        if not shipment_id:
            shipment_id = self._get_shipment_id(basket_id, shipment_id)

        return self._return_handler(self.resources.baskets.shipments_shipping_method_put(basket_id, shipment_id, shipping_method))

    def set_shipping_address(self, address, basket_id=None, shipment_id=None):
        """

        :type basket_id: str
        :type address: demandware.models.orderaddress.OrderAddress
        :rtype: demandware.models.basket.Basket
        """

        basket_id = basket_id

        if not shipment_id:
            shipment_id = self._get_shipment_id(basket_id, shipment_id)

        return self._return_handler(self.resources.baskets.shipments_shipping_address_put(basket_id, shipment_id, address))

    def set_billing_address(self, address, basket_id=None):
        """

        :type basket_id: str
        :type address: demandware.models.orderaddress.OrderAddress
        :rtype: demandware.models.basket.Basket
        """

        return self._return_handler(self.resources.baskets.billing_address_put(basket_id, address))

    def set_customer_info(self, input, basket_id=None):
        """

        Note: DW doesn't set the customer_name field through this call. If you pass it in as part of CustomerInfo,
        it get's ignored by DW

        :type basket_id: str
        :type input: demandware.models.customerinfo.CustomerInfo
        :rtype: demandware.models.basket.Basket
        """

        #TODO: Add ability to set logged in customer's email if one is not passed in
        return self._return_handler(self.resources.baskets.customer_put(basket_id, input))

    def apply_coupon(self, coupon_code, basket_id=None):

        return self._return_handler(self.resources.baskets.coupons_post(basket_id, {
            "code": coupon_code
        }))

    def remove_coupon(self, coupon_code, basket_id=None):

        return self._return_handler(self.resources.baskets.coupons_delete(basket_id, coupon_code))

    def fetch_customer_basket(self, customer_id):

        return self._return_handler(self.resources.customers.baskets_get(customer_id))

    def apply_gift_certificate(self, promotion_id, basket_id=None, amount=0):

        return self._return_handler(self.resources.baskets.gift_certificate_items_post(basket_id, {
            "gift_certificate_item_id": promotion_id,
            "amount": amount,
            "recipient_email": "TEST@TEST.COM",
            "recipient_name": "test"
        }))

    def submit_basket(self, basket_id=None, basket=None):

        #Demandware's docs allow you to pass in a whole basket to submit vs just the id.
        #It's optional but it allows us to pass in more information.
        #Validation is done in the next step
        ret = self._return_handler(self.resources.orders.post(dict({
            "basket_id": basket_id or self.basket_id,
        }, **dict(basket or {}))))
        #If successful, delete the old basket from the customer (we don't need it anymore) and set id to None
        if ret.status != 'failed':
            self.resources.baskets.delete(basket_id)
            if basket_id == None: # If no basket_id was passed, the above uses self.basket_id
                self.basket_id = None
        return ret

    def set_price_adjustment(self, price_adjustment, basket_id=None):
        return self._return_handler(
            self.resources.baskets.price_adjustments_post(basket_id=(basket_id), input=dict(price_adjustment)),
        )

    def remove_price_adjustment(self, price_adjustment_id, basket_id=None):
        return self._return_handler(
            self.resources.baskets.price_adjustments_delete(basket_id=(basket_id), price_adjustment_id=price_adjustment_id),
        )

    def set_storefront_basket(self, basket_id=None):
        cart = self._return_handler(
            self.resources.baskets.storefront_basket_put(basket_id=(basket_id))
        )
        self.basket_id = cart.basket_id
        return cart

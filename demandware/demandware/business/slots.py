from demandware.business import Demandware

class Slots(Demandware):
    """
    This Oauth Endpoint still maintain consistency like how calls are made via jwt endpoints
    Slots are is a data call in DW latest version. Clients like ardene have some endpoints that are slots
    """
    def fetch_slots(self, site_id, count=25, select="(**)", start=0):
        """
        :rtype: demandware.models of an  endpoint model
        :endpoints like trends in Ardene is a slot feature
        """
        return self.resources.slots.all_get(site_id=site_id, count=count, select=select, start=start)

    def fetch_slot(self, site_id, slot_id, context_type="global", select="(**)"):
        """
        :rtype: demandware.models of an  endpoint model
        :endpoints like trends in Ardene is a slot feature
        """
        return self.resources.slots.get(site_id=site_id, slot_id=slot_id, context_type=context_type, select=select)
from demandware import faults
from demandware.business import Demandware


class Customer(Demandware):

    def _handle_login_cart(fnc):
        def realfnc(self, *args, **kwargs):
            cart = None
            #Can't use self.basket_id here because it actually creates a basket if it doesn't exist
            if hasattr(self.resources.session, "demandware") and self.resources.session.demandware.get("basket_id"):
                try:
                    cart = self.fetch_cart()
                except (faults.DemandwareException):
                    cart = None # Fetching the cart failed in some fashion. Assume no guest cart.

            ret = fnc(self, *args, **kwargs)

            #If we have a guest basket with items in the cart, we'll need to assign it to the logged in user
            if cart and cart.product_items:
                #Unset the basket id so we don't try to delete the old guest cart we pulled before logging in
                self.basket_id = None
                self.assign_cart(basket=cart)

            return ret

        return realfnc

    @_handle_login_cart
    def login(self, username, password, translator=None):
        self.username = self.resources.username = username
        self.password = self.resources.password = password
        self.resources.login()
        return self.fetch_token_customer(translator)

    def logout(self):

        # Next call the JWTManager class will go through the token restablishment process
        self.resources.session.demandware = dict()

    @_handle_login_cart
    def create_customer(self, input, translator=None):
        """
        Create a new customer in demandware. Once a customer is created, it cannot be deleted.

        :rtype: demandware.xapi.models.customer.Customer
        :type input: demandware.xapi.models.customerregistration.CustomerRegistration | dict
        """

        return self._return_handler(self.resources.customers.post(dict(input)), translator)

    def update_customer_address(self, input, customer_id=None):
        input = dict(input)
        return self._return_handler(self.resources.customers.addresses_patch(customer_id=customer_id, address_name=input['address_id'], input=dict(input)))

    def fetch_token_customer(self, translator=None):
        """
        Fetches the customer that is in the jwt token. This allows you to obtain the currently logged in user.
        :rtype: demandware.xapi.models.customer.Customer
        """

        if self.resources.token:
            if not self.resources.token.guest:
                return self.fetch_customer(translator=translator) # TODO: Rework
        return None

    def fetch_customer(self, customer_id=None, translator=None):
        """
        Fetches a customer based on customer_id

        :type customer_id: str
        :rtype: demandware.xapi.models.customer.Customer
        """
        return self._return_handler(self.resources.customers.get(customer_id=customer_id), translator)

    def update_customer(self, input, customer_id=None):
        """

        :type customer_id: str
        :rtype: demandware.models.customer.Customer
        """

        return self._return_handler(self.resources.customers.patch(input=dict(input), customer_id=customer_id))

    def create_customer_address(self, address, customer_id=None):

        return self._return_handler(self.resources.customers.addresses_post(input=dict(address), customer_id=customer_id))

    def fetch_customer_addresses(self, customer_id=None, translator=None):

        return self._return_handler(self.resources.customers.addresses_all_get(customer_id=customer_id), translator)

    def remove_customer_address(self, address_name, customer_id=None, translator=None):
        return self._return_handler(self.resources.customers.addresses_delete(customer_id=customer_id, address_name=address_name), translator)

    def fetch_payment_instruments(self, customer_id=None):
        """

        :type basket_id: str
        :rtype: demandware.xapi.models.customerpaymentinstrumentresult.CustomerPaymentInstrumentResult
        """

        return self._return_handler(self.resources.customers.payment_instruments_all_get(customer_id))

    def create_payment_instrument(self, input, customer_id=None):
        """

        :type customer_id: str
        :rtype: demandware.xapi.models.customerpaymentinstrumentresult.CustomerPaymentInstrumentResult
        """

        return self._return_handler(self.resources.customers.payment_instruments_post(input=dict(input), customer_id=customer_id))

    def remove_payment_instrument(self, payment_instrument_id, customer_id=None):
        """

        :type customer_id: str
        :rtype: demandware.models.customer.Customer
        """

        #Input shoule be blank???
        return self._return_handler(self.resources.customers.payment_instruments_delete(payment_instrument_id=payment_instrument_id, customer_id=customer_id, input={}))

    def create_customer_product_list(self, input, customer_id=None, translator=None):
        """
        Creates a new product list for a customer
        """
        return self._return_handler(self.resources.customers.product_lists_post(input=dict(input), customer_id=customer_id), translator)

    def fetch_customer_product_lists_all(self, customer_id=None, translator=None):
        """
        Fetches a customer's product lists
        """
        ret = self.resources.customers.product_lists_all_get(customer_id=customer_id)
        self.customer_product_list_id = ret.data[0].id # Save the id of the first list in the session
        return self._return_handler(ret, translator)

    def fetch_customer_product_list(self, customer_id=None, list_id=None, translator=None):
        """
        Fetches a customer's product list either with the ID of the list_id parameter or self.customer_product_list_id
        """
        return self._return_handler(self.resources.customers.product_lists_get(customer_id=customer_id, list_id=list_id or self.customer_product_list_id), translator)

    def customer_product_list_add_item(self, input, customer_id=None, list_id=None, translator=None):
        return self._return_handler(self.resources.customers.product_lists_items_post(customer_id=customer_id, list_id=list_id or self.customer_product_list_id, input=dict(input)), translator)

    def customer_product_list_remove_item(self, item_id, customer_id=None, list_id=None, translator=None):
        return self._return_handler(self.resources.customers.product_lists_items_delete(customer_id=customer_id, list_id=list_id or self.customer_product_list_id, item_id=item_id), translator)

    def fetch_customer_order_history(self, customer_id=None, translator=None):
        """
        Fetches a customer's order history
        """
        return self._return_handler(self.resources.customers.orders_get(customer_id=customer_id), translator)

    def request_password_reset(self, input):
        """
        Requests a password reset, instructions will be sent to the customer's email address.
        :param input: A dictionary containing "type": ["email","login"] and "identification": which contains the string to match the type.
        :return: NoContent object on success.
        """
        return self.resources.customers.password_reset_post(input)

from demandware import Demandware, faults
from demandware.models.nocontent import NoContent
from demandware.tests.integration import utils

def test_password_reset_by_login():

    dw = Demandware(**utils.demandware_jwt_credentials())
    (_, username, _) = utils.register(dw)

    result = dw.request_password_reset({
        "type": "login",
        "identification": username,
    })

    assert type(result) is NoContent
    assert result._unknown_attributes == []

def test_password_reset_by_email():

    dw = Demandware(**utils.demandware_jwt_credentials())
    (_, username, _) = utils.register(dw)

    result = dw.request_password_reset({
        "type": "email",
        "identification": username,
    })

    assert type(result) is NoContent
    assert result._unknown_attributes == []

def test_password_reset_with_non_email():
    dw = Demandware(**utils.demandware_jwt_credentials())

    result = dw.request_password_reset({
        "type": "email",
        "identification": "~",
    })

    assert type(result) is NoContent
    assert result._unknown_attributes == []

def test_password_reset_with_empty_login():
    dw = Demandware(**utils.demandware_jwt_credentials())

    result = dw.request_password_reset({
        "type": "login",
        "identification": "",
    })
    assert type(result) is NoContent
    assert result._unknown_attributes == []

def test_password_reset_with_empty_email():
    dw = Demandware(**utils.demandware_jwt_credentials())

    try:
        result = dw.request_password_reset({
            "type": "email",
            "identification": "",
        })
        assert False # Should not reach here
    except (faults.DemandwareException) as raised:
        assert raised

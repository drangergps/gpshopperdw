from demandware import Demandware
from demandware.tests.integration import utils


def test_fetch_categories():

    dw = Demandware(**utils.demandware_no_credentials())
    browse = dw.fetch_categories()

    assert browse.categories
    assert browse.id == 'root'



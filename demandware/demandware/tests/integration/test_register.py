from demandware import Demandware, faults
from demandware.tests.integration import utils


def test_create_new_user():

    dw = Demandware(**utils.demandware_jwt_credentials())
    guest_token = dw.resources.token

    (result, username, password) = utils.register(dw)

    assert result.auth_type == "registered", "Created a new user, but they are not marked as registered"
    assert result.customer_id, "No customer id"

    # Attempt a login
    dw.login(username, password)
    token = dw.resources.token

    assert token != guest_token

    token_customer = dw.fetch_token_customer()

    assert token_customer.customer_id == result.customer_id

def test_create_same_user():
    dw1 = Demandware(**utils.demandware_jwt_credentials())
    (result, username, password) = utils.register(dw1)

    # Attempt registering again in a fresh instance.
    dw2= Demandware(**utils.demandware_jwt_credentials())
    try:
        (result, username, password) = utils.register(dw2, username, password)
    except (faults.LoginAlreadyInUseException):
        assert True # Should raise this exception
    else:
        assert False # Should not reach here

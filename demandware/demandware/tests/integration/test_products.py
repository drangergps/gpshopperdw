from demandware import Demandware
from demandware.tests.integration.utils import demandware_no_credentials
from demandware.models import Product

def test_product_obtain():

    dw = Demandware(**demandware_no_credentials())
    search_result = dw.search(input={
        "q": "red",
        "count": 1
    })

    product_info = dw.fetch_product_details(product_id=search_result.hits[0].product_id)

    assert product_info.id == search_result.hits[0].product_id

def test_product_recommendations():

    dw = Demandware(**demandware_no_credentials())
    search_result = dw.search(input={
        "q": "red",
        "count": 1
    })

    product_recommendations = dw.fetch_product_recommendations(product_id=search_result.hits[0].product_id)

    assert product_recommendations.id == search_result.hits[0].product_id
    #Just the fact that we don't get an error and the return type is the correct model is all we can guarantee
    #In the way of recommendations being populated
    assert type(product_recommendations) == Product

from demandware import Demandware, models
from core.model import strip
from demandware.tests.integration import utils


def test_guest_fetch_saved_addresses():
    dw = Demandware(**utils.demandware_jwt_credentials())

    result = dw.fetch_customer_addresses()

    assert isinstance(result, models.CustomerAddressResult)

def test_user_update_saved_address():
    dw = Demandware(**utils.demandware_jwt_credentials())
    (result, username, password) = utils.register(dw)
    customer_profile = dw.login(username, password)

    address = strip(models.CustomerAddress(
        address1="1 Test Lane",
        address2="apt 1",
        address_id="1testlane",
        city="Chicago",
        company_name="GPS",
        country_code="US",
        first_name="Ken",
        full_name="Ken Cross",
        job_title="Integrator",
        last_name="Cross",
        phone="5558675309",
        postal_code="60602",
        state_code="IL",
        #For some reason these are in the model but we can't pass them in...
        creation_date=None,
        last_modified=None,
    ))
    result = dw.create_customer_address(address)

    address.city = "New York"
    result = dw.update_customer_address(input=address)

    assert isinstance(result, models.CustomerAddress)
    assert result.address_id == "1testlane"
    assert result.city == "New York"

def test_user_delete_saved_address():
    dw = Demandware(**utils.demandware_jwt_credentials())
    (result, username, password) = utils.register(dw)
    customer_profile = dw.login(username, password)

    address = strip(models.CustomerAddress(
        address1="1 Test Lane",
        address2="apt 1",
        address_id="1testlane",
        city="Chicago",
        company_name="GPS",
        country_code="US",
        first_name="Ken",
        full_name="Ken Cross",
        job_title="Integrator",
        last_name="Cross",
        phone="5558675309",
        postal_code="60602",
        state_code="IL",
        #For some reason these are in the model but we can't pass them in...
        creation_date=None,
        last_modified=None,
    ))
    result = dw.create_customer_address(address)

    result = dw.remove_customer_address(address_name=result.address_id)

    assert isinstance(result, models.Customer)

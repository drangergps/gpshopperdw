from demandware import Demandware
from demandware.tests.integration import utils

import os
import decimal

if os.environ.get("DW_TEST_STORES"):

    def test_get_all_stores():

        dw = Demandware(**utils.demandware_no_credentials())

        result = dw.fetch_all_stores_by_zip(postal_code="60610", country_code="US")

        assert result

    def test_get_store():

        dw = Demandware(**utils.demandware_no_credentials())

        result = dw.fetch_all_stores_by_zip(postal_code="60610", country_code="US", max_distance=20012)

        result = dw.fetch_store_by_id(result['data'][0].id)

        assert result

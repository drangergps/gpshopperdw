from demandware import Demandware, models
from demandware.tests.integration import utils


def test_guest_fetch_payment_instruments():
    dw = Demandware(**utils.demandware_jwt_credentials())

    result = dw.fetch_payment_instruments()

    assert isinstance(result, models.CustomerPaymentInstrumentResult)

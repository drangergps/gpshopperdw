from demandware.business import SearchQuery
from demandware import Demandware
from demandware.tests.integration import utils

import decimal

def test_raw_search():

    dw = Demandware(**utils.demandware_no_credentials())

    query = SearchQuery(q="blue")
    result = dw.search({
        "q": "red",
        "refine_1": "c_color=red"
    })

    for item in result.hits:
        assert item.price != decimal.Decimal("0.0"), 'Item price came back as 0'

def test_filter_price_range():

    min = 100
    max = 500

    dw = Demandware(**utils.demandware_no_credentials())

    query = SearchQuery(q="red")
    query.add_refine_range("price", (min, max))
    result = dw.search(query)

    for item in result.hits:
        assert item.price < min or item.price > max, 'Item price came back as 0'
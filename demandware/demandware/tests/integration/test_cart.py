from demandware import Demandware
from demandware.models import Basket, \
    ProductItem, OrderAddress, CustomerInfo

from demandware.tests.integration import utils


def add_item_to_cart(dw):

    search = dw.search({"q": "red", "count": 10})
    product, child = utils.find_orderable_product(dw, search, 0)

    cart = dw.create_cart(Basket())

    cart = dw.cart_add_product(basket_id=cart.basket_id, product_item=ProductItem(
        quantity=1,
        product_id=child.product_id
    ))

    return cart, product, child

def test_create_cart():

    dw = Demandware(**utils.demandware_jwt_credentials())

    # Empty basket
    result = dw.create_cart(Basket())

    assert result.basket_id

def test_add_item_to_basket():

    dw = Demandware(**utils.demandware_jwt_credentials())

    cart, product, child = add_item_to_cart(dw)

    assert cart.product_items[0].product_id == child.product_id

    cart = dw.cart_remove_product(cart.product_items[0].item_id, cart.basket_id)

    cart = dw.fetch_cart(basket_id=cart.basket_id)

    assert cart.product_items == []

def test_merge_carts():

    dw = Demandware(**utils.demandware_jwt_credentials())

    cart, product, child = add_item_to_cart(dw)

    (customer, username, password) = utils.register(dw)

    new_cart = dw.fetch_cart()

    assert cart.basket_id != new_cart.basket_id
    assert cart.product_items[0].product_id == new_cart.product_items[0].product_id

def test_payment_methods():

    dw = Demandware(**utils.demandware_jwt_credentials())

    cart, product, child = add_item_to_cart(dw)
    methods = dw.fetch_payment_methods(cart.basket_id)

    assert methods.applicable_payment_methods != []


def test_add_payment():

    dw = Demandware(**utils.demandware_jwt_credentials())

    cart, product, child = add_item_to_cart(dw)

    cart = dw.set_payment(utils.payment_request, cart.basket_id)

    assert cart.payment_instruments[0].payment_card.number_last_digits == utils.payment_request.payment_card.number[-4:]

def test_add_shipping_method():

    dw = Demandware(**utils.demandware_jwt_credentials())

    cart, product, child = add_item_to_cart(dw)

    shipping_methods = dw.fetch_shipping_methods(cart.basket_id, cart.shipments[0].shipment_id)

    assert shipping_methods.default_shipping_method_id
    assert shipping_methods.applicable_shipping_methods != []

    cart = dw.set_shipping_method(shipping_methods.applicable_shipping_methods[0], cart.basket_id, cart.shipments[0].shipment_id)

    assert cart.shipments[0].shipping_method.id == shipping_methods.applicable_shipping_methods[0].id

def test_set_shipping_address():

    dw = Demandware(**utils.demandware_jwt_credentials())

    cart, product, child = add_item_to_cart(dw)

    address = utils.basic_address
    cart = dw.set_shipping_address(address, cart.basket_id)

    assert cart.shipments[0].shipping_address.first_name == address.first_name
    assert cart.shipments[0].shipping_address.last_name == address.last_name
    assert cart.shipments[0].shipping_address.address1 == address.address1
    assert cart.shipments[0].shipping_address.city == address.city
    assert cart.shipments[0].shipping_address.country_code == address.country_code
    assert cart.shipments[0].shipping_address.phone == address.phone
    assert cart.shipments[0].shipping_address.postal_code == address.postal_code

def test_set_billing_address():

    dw = Demandware(**utils.demandware_jwt_credentials())

    cart, product, child = add_item_to_cart(dw)

    address = OrderAddress(
        first_name="GPShopper",
        last_name="GPShopper",
        address1="33 North Lasalle",
        city="Chicago",
        country_code="USA",
        phone="1231231234",
        postal_code="60602"
    )

    cart = dw.set_billing_address(address, cart.basket_id)

    assert cart.billing_address.first_name == address.first_name
    assert cart.billing_address.last_name == address.last_name
    assert cart.billing_address.address1 == address.address1
    assert cart.billing_address.city == address.city
    assert cart.billing_address.country_code == address.country_code
    assert cart.billing_address.phone == address.phone
    assert cart.billing_address.postal_code == address.postal_code

def test_set_customer_info():

    dw = Demandware(**utils.demandware_jwt_credentials())

    cart, product, child = add_item_to_cart(dw)

    customer_info = CustomerInfo(
        customer_id=dw.customer_id,
        email="test@gpshopper.com"
    )

    cart = dw.set_customer_info(customer_info, cart.basket_id)

    assert cart.customer_info.customer_id == dw.customer_id
    assert cart.customer_info.email == customer_info.email

def test_basket_id_get():
    dw1 = Demandware(**utils.demandware_jwt_credentials())
    cart1, product, child = add_item_to_cart(dw1)

    dw2 = Demandware(**utils.demandware_jwt_credentials(session=dw1.resources.session))

    cart2 = dw2.fetch_cart()

    assert dw1.basket_id == dw2.basket_id

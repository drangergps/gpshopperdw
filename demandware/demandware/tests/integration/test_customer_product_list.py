from demandware import Demandware, models
from demandware.tests.integration import utils


def add_item_to_customer_product_list(dw):
    search = dw.search({"q": "red", "count": 10})
    product, child = utils.find_orderable_product(dw, search, 0)

    customer_product_list_item = dw.customer_product_list_add_item({"product_id":product.id, "type":"product"})

    return customer_product_list_item, product, child


def test_create_guest_create_customer_product_list():
    dw = Demandware(**utils.demandware_jwt_credentials())

    result = dw.create_customer_product_list({"type":"wish_list"})

    assert isinstance(result, models.CustomerProductList)
    assert result.id

def test_guest_auto_generate_customer_product_list():
    dw = Demandware(**utils.demandware_jwt_credentials())

    touch = dw.customer_product_list_id

    assert dw.customer_product_list_id

def test_guest_customer_product_list_add():
    dw = Demandware(**utils.demandware_jwt_credentials())

    result, product, child = add_item_to_customer_product_list(dw)

    assert isinstance(result, models.CustomerProductListItem)
    assert result.product_id == product.id

    # Now verify it's actually added
    customer_product_list = dw.fetch_customer_product_list()


    assert customer_product_list.customer_product_list_items[0].product_id == product.id


def test_guest_customer_product_list_remove():
    dw = Demandware(**utils.demandware_jwt_credentials())

    add_result, product, child = add_item_to_customer_product_list(dw)


    dw.customer_product_list_remove_item(add_result.id)

    customer_product_list = dw.fetch_customer_product_list()

    assert len(customer_product_list.customer_product_list_items) == 0


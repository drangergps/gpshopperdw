import os
from random import Random

from demandware.models import customerregistration, customer, BasketPaymentInstrumentRequest, \
    OrderPaymentCardRequest, OrderAddress

from demandware.business import AuthorizationSchemes
from demandware.jwt_manager import JwtManager
from demandware.oauth_manager import OauthManager
from demandware.no_manager import NoManager

#It is worth noting here that with this pattern even if your code doesn't use JWT or Oauth
#(ex. if you do browse or search only)
#You must still initialize the class with a manager. However, as of refactoring to the managers pattern, you need this anyway.
#This latest iteration just makes that need more explicit
def demandware_jwt_credentials(session=None):
    return {
        "managers": [JwtManager(
            base_url=os.environ.get("DW_URL"),
            client_id=os.environ.get("DW_CLIENT_ID"),
            session=session
        )],
        "default_authorization_scheme": AuthorizationSchemes.JWT,
    }
def demandware_oauth_credentials(session=None):
    return {
    "managers": [OauthManager(
        base_url=os.environ.get("DW_OAUTH_URL") or os.environ.get("DW_URL"),
        client_id=os.environ.get("DW_OAUTH_CLIENT_ID") or os.environ.get("DW_CLIENT_ID"),
        client_secret=os.environ.get("DW_CLIENT_SECRET"),
        session=session
    )],
    "default_authorization_scheme": AuthorizationSchemes.OAUTH,
}
def demandware_no_credentials():
    return {
        "managers": [NoManager(
            base_url=os.environ.get("DW_URL"),
            client_id=os.environ.get("DW_CLIENT_ID"),
        )],
        "default_authorization_scheme": AuthorizationSchemes.NONE,
    }
def demandware_all_credentials(session=None):
    return {
        "managers": [
            NoManager(
                base_url=os.environ.get("DW_URL"),
                client_id=os.environ.get("DW_CLIENT_ID"),
            ),
            OauthManager(
                base_url=os.environ.get("DW_URL"),
                client_id=os.environ.get("DW_CLIENT_ID"),
                session=session
            ),
            JwtManager(
                base_url=os.environ.get("DW_URL"),
                client_id=os.environ.get("DW_CLIENT_ID"),
                session=session
            )
        ],
        "default_authorization_scheme": AuthorizationSchemes.NONE,
    }

payment_request = BasketPaymentInstrumentRequest(
    payment_method_id="CREDIT_CARD",
    payment_card=OrderPaymentCardRequest(
        card_type="Visa",
        expiration_month=10,
        expiration_year=10,
        holder="Test test",
        number="4111111111111111",
        security_code="111"
    )
)

basic_address = OrderAddress(
    first_name="GPShopper",
    last_name="GPShopper",
    address1="33 North Lasalle",
    city="Chicago",
    country_code="USA",
    phone="1231231234",
    postal_code="60602"
)

def register(dw, username=None, password=None):

    random = Random()

    username = username or str(random.randint(0, 100000)) + "@" + str(random.randint(0, 100000)) + "gpshopper.com"
    password = password or "test123abs456!"

    result = dw.create_customer(customerregistration.CustomerRegistration(
        password=password,
        customer=customer.Customer(
            login=username,
            email=username,
            first_name="GPSHOPPER",
            last_name="GPSHOPPER"
        )
    ))
    print("Registered with %s and pass %s" % (username, password))

    return (result, username, password)


def find_orderable_product(dw, search, index):
    if index >= len(search.hits):
        raise Exception("Cannot find product to add to cart")

    product = dw.fetch_product(search.hits[index].product_id)
    result = [variant for variant in product.variants if variant.orderable]
    if not result:
        find_orderable_product(search, index + 1)

    return product, result[0]

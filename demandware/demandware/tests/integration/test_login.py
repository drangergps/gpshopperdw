from demandware import Demandware, faults
from demandware.tests.integration import utils

def test_login():

    dw = Demandware(**utils.demandware_jwt_credentials())
    (result, username, password) = utils.register(dw)
    customer_profile = dw.login(username, password)

    assert customer_profile
    assert result.customer_id
    assert customer_profile.customer_id == result.customer_id
    assert result.login == username

def test_invalid_login():
    dw = Demandware(**utils.demandware_jwt_credentials())
    try:
        invalid_profile = dw.login("INVALID@USER","invalid_user")
        assert False # Should not reach here
    except (faults.DemandwareException) as raised:
        assert raised

def test_token_saved_into_session():

    session = type('session', (object,), {})()

    dw = Demandware(**utils.demandware_jwt_credentials(session=session))
    utils.register(dw)

    assert session.demandware["jwt_token"] != None

def test_token_fetch_customer_not_logged_in():

    dw = Demandware(**utils.demandware_jwt_credentials())
    result = dw.fetch_token_customer()

    pass
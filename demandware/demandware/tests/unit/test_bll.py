import unittest2

from demandware.business import SearchQuery

class Tests(unittest2.TestCase):

    def test_search_refine(self):
        query = SearchQuery()
        query.add_refine_param({"foo": "bar"})

        self.assertEqual(query.refine, ["foo=bar"])

    def test_search_refine_with_items(self):

        query = SearchQuery()
        query.add_refine_param({"foo": ("lorem", "ipsum")})

        self.assertEqual(query.refine, ["foo=lorem|ipsum"])

    def test_search_refine_range(self):

        query = SearchQuery()
        query.add_refine_range("price", (500, 1000))

        self.assertEqual(query.refine, ["price=(500..1000)"])

    def test_search_refine_with_one_item(self):

        query = SearchQuery()
        query.add_refine_range("price", (500,))

        self.assertEqual(query.refine, ["price=500"])
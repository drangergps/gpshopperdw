from demandware.business import cart, product, customer, search, store, browse, order, slots
from demandware.jwt_manager import JwtManager
from demandware.oauth_manager import OauthManager

#Facade
class Demandware(cart.Cart, product.Product, customer.Customer, search.Search, store.Store, browse.Browse, order.Order, slots.Slots):
    pass

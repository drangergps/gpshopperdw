import copy
from six.moves.urllib.parse import urlencode


def make_basket_submittable(basket):
    """
    Demandware does not allow you to submit a basket that was obtained in another call. This method
    removes values that demandware does not accept.

    :type basket: demandware.xapi.models.basket.Basket
    """
    basket = copy.copy(basket)
    basket.shipments = []


    # If the item is a bonus product and if we assign the cart to a user,
    # then Demandware will attempt to add the item that triggers the bonus product AND the product itself, leading to quantity 2 of the bonus item in cart.
    for product in list(basket.product_items):
        if product.bonus_product_line_item:
            basket.product_items.remove(product)

    for instrument in basket.payment_instruments:
        del instrument.authorization_status

    return basket

def build_args(locals, **kwargs):

    combined_args = dict(locals)
    combined_args.update(kwargs)
    del combined_args['self']
    del combined_args['kwargs']

    return urlencode({
        key: value for key, value in combined_args.items() if value
    })

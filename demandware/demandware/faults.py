class DemandwareException(Exception):
    def __init__(self, message, arguments):
        self.message = message
        self.arguments = arguments
        super(DemandwareException, self).__init__(message, arguments)


class HookStatusException(DemandwareException):
    message = "Error in ExtensionPoint."


class AuthenticationFailedException(DemandwareException):
    message = "Customer authentication based on Authorization:Basic header failed."


class ConstraintViolationException(DemandwareException):
    message = "'count' value constraint violated. Expected value is between '(1..200)'."


class StringConstraintViolationException(DemandwareException):
    message = "The string length constraint was violated by 'query' param. The expected length is '(3..50)'."


class ValueConstraintViolationException(DemandwareException):
    message = "'count' violates the value constraint. The expected value is between '(1..10)'."


class ExpiredTokenException(DemandwareException):
    message = "The provided token has expired."


class AddressIdAlreadyInUseException(DemandwareException):
    message = "provided address name is already used for the customer."


class IllegalHttpMethodOverrideException(DemandwareException):
    message = "Only HTTP methods 'PUT', 'PATCH', 'DELETE' can be overwritten."


class InvalidBillingAddressException(DemandwareException):
    message = "Invalid billing address."


class InvalidBundledProductItemCountException(DemandwareException):
    message = "Number of update bundled product items doesn't match number of server bundled product items."


class InvalidCouponItemException(DemandwareException):
    message = "Coupon code 'SUMMER2012' is invalid."


class CustomerBasketsQuotaExceededException(DemandwareException):
    message = "Maximum number of baskets per customer quota exceeded."


class CouponItemNotFoundException(DemandwareException):
    message = "Coupon item not found."


#Hmm this isn't in the docs but it does get raised...
class InvalidCouponCodeException(DemandwareException):
    message = "Coupon code is invalid."


class InvalidCustomerException(DemandwareException):
    message = "Invalid customer."


class BasketNotFoundException(DemandwareException):
    message = "Basket not found."


class InvalidAccessTokenException(DemandwareException):
    message = "Invalid token"


class CustomerNotFoundException(DemandwareException):
    message = "Customer is not found."


class InvalidCustomerInformationException(DemandwareException):
    message = "Invalid customer info 'email' address."


class InvalidCustomPropertyException(DemandwareException):
    message = "Invalid custom property in request."


class InvalidExpandParameterException(DemandwareException):
    message = "Invalid expand parameter 'foo' found."


class InvalidMessageException(DemandwareException):
    message = "Mandatory attribute 'email' must not be null or empty"


class InvalidOptionItemException(DemandwareException):
    message = "Product option 'warranty' is not supported."


class InvalidPasswordException(DemandwareException):
    message = "Password doesn't match acceptance criteria."


class InvalidPaymentMethodException(DemandwareException):
    message = "Payment method with id 'Paypal' is unknown or not applicable to basket."


class InvalidProductItemException(DemandwareException):
    message = "Product '0815' is unknown, offline or not assigned to site catalog."


class InvalidShippingAddressException(DemandwareException):
    message = "Invalid shipping address."


class InvalidShippingMethodException(DemandwareException):
    message = "Shipping method with id 'UPS' is unknown or not applicable to basket."


class InvalidUsernameException(DemandwareException):
    message = "Username doesn't match acceptance criteria."


class LoginAlreadyInUseException(DemandwareException):
    message = "The login is already in use."


class MalformedLocaleException(DemandwareException):
    message = "Malformed locale value 'en+US'."


class MalformedMediaTypeException(DemandwareException):
    message = "Malformed Content-Type 'foo' in header."


class MalformedParameterException(DemandwareException):
    message = "Malformed value '10p' for parameter 'count'."


class MalformedPathVariableException(DemandwareException):
    message = "Malformed path variable '(123,456'. The correct syntax for multiple ids is: (id1,id2,id3). If the characters ( ) , are part of the id itself, they have to be URL encoded."


class MalformedPriceRefinementException(DemandwareException):
    message = "Malformed price refinement '(0..5x)'. Expected something like '(0..100)'."


class MalformedSelectorException(DemandwareException):
    message = "Malformed property selector '(name,id'."


class MissingClientIdException(DemandwareException):
    message = "Missing client id."


class MissingParameterException(DemandwareException):
    message = "Missing parameter 'campaign_id'."


class ProductItemNotAvailableException(DemandwareException):
    message = "Product '017845724321' is not available for quantity '5'."


class QuotaExceededException(DemandwareException):
    message = "Maximum number of product items per basket exceeded."


class UnknownLocaleException(DemandwareException):
    message = "The locale 'foo is unknown."


class UnknownParameterException(DemandwareException):
    message = "Unknown parameter 'foo'."


class UnsupportedLocaleException(DemandwareException):
    message = "The locale 'de-DE' is not supported/activated for this site."


class UsernameAlreadyInUseException(DemandwareException):
    message = "Username is already in use."


class CustomerAlreadyRegisteredException(DemandwareException):
    message = "Customer is already registered."


class InvalidSecureTokenException(DemandwareException):
    message = "Session may have been hijacked."


class UnauthorizedException(DemandwareException):
    message = "Unauthorized request for resource '/s/SiteGenesis/dw/shop/v17_7/basket/this/checkout/submit' from client 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'."


class UnauthorizedOriginException(DemandwareException):
    message = "Unauthorized origin 'foo.com' from client 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'."


class UnknownClientIdException(DemandwareException):
    message = "Unknown client id 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb'."


class SecureCommunicationRequiredException(DemandwareException):
    message = "Secure communication required."


class SiteOfflineException(DemandwareException):
    message = "The site 'SiteGenesis' is currently offline and can''t be accessed."


class InvalidVersionException(DemandwareException):
    message = "Invalid version syntax 'v17.7'. Please provide a valid version like 'v17_7'."


class NotFoundException(DemandwareException):
    message = "No product with id '0815' for site 'SiteGenesis' found."


class ResourcePathNotFoundException(DemandwareException):
    message = "If the requested URL matches the Commerce Cloud Digital Rest URL pattern, but the resource path is unknown i.e. /dw/shop/v17_7/unknown/123."


class UnknownVersionException(DemandwareException):
    message = "Unknown version 'v33.33'. Please provide a valid version."


class MethodNotAllowedException(DemandwareException):
    message = "Method 'DELETE' not allowed."


class NotAcceptableException(DemandwareException):
    message = "Unsupported charset 'iso-8859-1' in Accept-Charset header. Note: Only UTF-8 charset is supported."


class RequestEntityTooLargeException(DemandwareException):
    message = "Request body size limit of 5 MB has been exceeded."


class RequestUriTooLongException(DemandwareException):
    message = "Request URL length limit of 2000 characters has been exceeded."


class UnsupportedMediaTypeException(DemandwareException):
    message = "Unsupported document format 'pdf'."


class InternalErrorException(DemandwareException):
    message = "Internal Server Error."


class AuthorizationHeaderMissingException(DemandwareException):
    message = ""


lookup = {
    'HookStatusException': HookStatusException,
    'AuthenticationFailedException': AuthenticationFailedException,
    'AddressIdAlreadyInUseException': AddressIdAlreadyInUseException,
    'ConstraintViolationException': ConstraintViolationException,
    'StringConstraintViolationException': StringConstraintViolationException,
    'ValueConstraintViolationException': ValueConstraintViolationException,
    'ExpiredTokenException': ExpiredTokenException,
    'IllegalHttpMethodOverrideException': IllegalHttpMethodOverrideException,
    'InvalidBillingAddressException': InvalidBillingAddressException,
    'InvalidBundledProductItemCountException': InvalidBundledProductItemCountException,
    'InvalidCouponItemException': InvalidCouponItemException,
    'InvalidCouponCodeException': InvalidCouponCodeException,
    'InvalidCustomerException': InvalidCustomerException,
    'CouponItemNotFoundException': CouponItemNotFoundException,
    'BasketNotFoundException': BasketNotFoundException,
    'InvalidCustomerInformationException': InvalidCustomerInformationException,
    'InvalidCustomPropertyException': InvalidCustomPropertyException,
    'InvalidExpandParameterException': InvalidExpandParameterException,
    'InvalidMessageException': InvalidMessageException,
    'InvalidOptionItemException': InvalidOptionItemException,
    'InvalidPasswordException': InvalidPasswordException,
    'InvalidPaymentMethodException': InvalidPaymentMethodException,
    'InvalidProductItemException': InvalidProductItemException,
    'InvalidShippingAddressException': InvalidShippingAddressException,
    'InvalidShippingMethodException': InvalidShippingMethodException,
    'InvalidUsernameException': InvalidUsernameException,
    'LoginAlreadyInUseException': LoginAlreadyInUseException,
    'MalformedLocaleException': MalformedLocaleException,
    'MalformedMediaTypeException': MalformedMediaTypeException,
    'MalformedParameterException': MalformedParameterException,
    'MalformedPathVariableException': MalformedPathVariableException,
    'MalformedPriceRefinementException': MalformedPriceRefinementException,
    'MalformedSelectorException': MalformedSelectorException,
    'MissingClientIdException': MissingClientIdException,
    'MissingParameterException': MissingParameterException,
    'ProductItemNotAvailableException': ProductItemNotAvailableException,
    'QuotaExceededException': QuotaExceededException,
    'UnknownLocaleException': UnknownLocaleException,
    'UnknownParameterException': UnknownParameterException,
    'UnsupportedLocaleException': UnsupportedLocaleException,
    'UsernameAlreadyInUseException': UsernameAlreadyInUseException,
    'InvalidSecureTokenException': InvalidSecureTokenException,
    'UnauthorizedException': UnauthorizedException,
    'UnauthorizedOriginException': UnauthorizedOriginException,
    'UnknownClientIdException': UnknownClientIdException,
    'SecureCommunicationRequiredException': SecureCommunicationRequiredException,
    'SiteOfflineException': SiteOfflineException,
    'InvalidVersionException': InvalidVersionException,
    'InvalidAccessTokenException': InvalidAccessTokenException,
    'NotFoundException': NotFoundException,
    'ResourcePathNotFoundException': ResourcePathNotFoundException,
    'UnknownVersionException': UnknownVersionException,
    'MethodNotAllowedException': MethodNotAllowedException,
    'NotAcceptableException': NotAcceptableException,
    'RequestEntityTooLargeException': RequestEntityTooLargeException,
    'RequestUriTooLongException': RequestUriTooLongException,
    'UnsupportedMediaTypeException': UnsupportedMediaTypeException,
    'InternalErrorException': InternalErrorException,
    'AuthorizationHeaderMissingException': AuthorizationHeaderMissingException,
    'CustomerNotFoundException': CustomerNotFoundException,
    'CustomerBasketsQuotaExceededException': CustomerBasketsQuotaExceededException,
    'CustomerAlreadyRegisteredException': CustomerAlreadyRegisteredException,
}
from demandware import resources, faults
import logging, simplejson, time, requests,json
from rauth import OAuth2Service
import base64
import inspect
import logging
import six
logger = logging.getLogger(__name__)

"""For endpoints using oauth2 implementation"""

global_logger = logging.getLogger(__name__)


class OauthManager(object):

    #These attributes can be accessed via the business layer
    #TODO: Add the rest of the resources
    resource_classes = {
        "baskets": resources.baskets.Api,
        "categories": resources.categories.Api,
        "content": resources.content.Api,
        "content_search": resources.content_search.Api,
        "custom_object": resources.custom_object.Api,
        "customers": resources.customers.Api,
        "gift_certificate": resources.gift_certificate.Api,
        "order_search": resources.order_search.Api,
        "orders": resources.orders.Api,
        "product_lists": resources.product_lists.Api,
        "product_search": resources.product_search.Api,
        "products": resources.products.Api,
        "promotions": resources.promotions.Api,
        "search_suggestions": resources.search_suggestion.Api,
        "sessions": resources.sessions.Api,
        "site": resources.site.Api,
        "slots": resources.slots.Api,
        "stores": resources.stores.Api,
    }

    def __init__(self, base_url, client_id, session=None, client_secret=None, access_token_url="", authorize_url="", username=None, password=None, oauth_username=None, oauth_password=None, locale=None, currency=None, pretty_print=None):

        self.session = session or type('session', (object,), {})()
        session_token = getattr(self.session, "demandware", {}).get("oauth_token", "")
        if session_token:
            self.access_token = session_token
        self.client_id = client_id
        self.username = username
        self.password = password
        self.client_secret = client_secret
        self.oauth_username = oauth_username
        self.oauth_password = oauth_password
        self.logger = logger or global_logger
        #https://account.demandware.com/dw/oauth2/access_token is a standard URL for OCAPI:
        self.access_token_url = access_token_url or "https://account.demandware.com/dw/oauth2/access_token"
        self.authorize_url = authorize_url
        self.base_url = base_url
        self.currency = currency
        self.locale = locale
        self.pretty_print = pretty_print

        #TODO: Add validation that the appropriate parameters are passed to initialize the service
        self.service = OAuth2Service(
            client_id=self.client_id,
            client_secret=self.client_secret,
            access_token_url=self.access_token_url,
            authorize_url=self.authorize_url,
            base_url=self.base_url,
        )

        for resource_name, resource_class in self.resource_classes.items():
            setattr(self, resource_name, self._resource_wrapper(resource_class))

    #Get headers specifically for oauth
    def get_headers(self, client_id, access_token=None):
        headers = {
            "x-dw-client-id": client_id,
            "content-Type": "application/x-www-form-urlencoded",
        }
        if access_token:
            headers["Authorization"] = "Basic " + access_token.decode("utf-8")
        return headers

    def fetch_access_token(self):

        if self.oauth_username and self.oauth_password:
            data = {
                'grant_type' : "urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken",
                'client_id' : self.client_id
            }
            headers = self.get_headers(
                client_id = self.client_id,
                access_token = base64.b64encode("{oauth_user_id}:{oauth_user_password}:{oauth_client_password}".format(
                    oauth_user_id=self.oauth_username,
                    oauth_user_password=self.oauth_password,
                    oauth_client_password=self.client_secret,
                ).encode()),
            )
        else:
            data = {
                'grant_type' : 'client_credentials',
            }
            headers = self.get_headers(
                client_id = self.client_id,
            )
        try:
            if self.service.access_token_url != "":
                token = self.service.get_access_token(
                    data=data,
                    decoder=lambda s: json.loads(s.decode('utf-8')),
                    headers=headers
                )
                return token
            else:
                logger.level("Empty access token url. Please make sure the token url is in the DB or defined in the code",
                             exc_info=True)

        except requests.HTTPError as e:
            logger.level("Could not get token", exc_info=True)
            raise

    @property
    def access_token(self):
        try:
            return self._access_token
        except (AttributeError):
            raw_token = getattr(self.session, "demandware", {}).get("oauth_token", "")
            if not raw_token:
                self.access_token = self.fetch_access_token()
            return self.access_token

    @access_token.setter
    def access_token(self, new_access_token):
        if new_access_token is None:
            self._access_token = None
            if getattr(self.session, "demandware", {}).get("oauth_token"):
                self.session.demandware["oauth_token"] = ""
        else:
            self._access_token = new_access_token
            if hasattr(self.session, "demandware"):
                self.session.demandware["oauth_token"] = new_access_token

    def _resource_wrapper(self, resource_class):

        # Create an instance of resource_class
        resource_instance = resource_class(
            base_url = self.base_url,
            client_id = self.client_id,
            locale = self.locale,
            currency = self.currency,
        )

        def token_wrapper(func):
            def wrapped(*args, **kwargs):
                try:
                    return func(*args, token=self.access_token if self.access_token else None, **kwargs)
                except (faults.ExpiredTokenException, faults.InvalidAccessTokenException,
                        faults.AuthorizationHeaderMissingException):
                    self.logger.debug("Attempted to use token, but it was expired.")
                    self.access_token = self.fetch_access_token()
                    self.logger.debug("Successfully re-authed after request.")
                    return func(*args, token=self.access_token if self.access_token else None, **kwargs)

            return wrapped

        for name, func in inspect.getmembers(resource_instance, inspect.ismethod):
            arg_list = inspect.getargspec(func).args if six.PY2 else inspect.getfullargspec(func).args
            new_func = func
            if "token" in arg_list and not name.startswith("_"):

                # Token is the outermost wrapper, as its recovery conditions could change the others.
                new_func = token_wrapper(new_func)

                setattr(resource_instance, name, new_func)
            # else leave the function alone

        return resource_instance

import curlify
import copy
from demandware import faults
from six.moves.urllib import parse as urlparse
import requests
import requests.auth
try:
    from gpshopper import logger
    logger = logger.getLogger(__name__)
#If we use this somewhere where gpshopper isn't present...
except:
    import logging
    logger = logging.getLogger(__name__)

def filter_args(args):
    '''
    :param args:
    :return: Filtered list of args that is ready to be sent to Demandware. Removes self and kwargs as parameters
    '''
    return {
        arg_name: arg_value
        for arg_name, arg_value in args.items()
        if arg_value and arg_name not in ("self", "kwargs")
    }

def build_list_args(*args):
    return [str(arg) for arg in args]

def check_fault(result):
    try:
        result = result.json()
        if result.get("fault"):
            raise faults.lookup.get(result["fault"]["type"], faults.DemandwareException)(
                message=result["fault"]["message"],
                arguments=result["fault"].get('arguments', {}),
            )
        return result
    except requests.HTTPError as e:
        logger.error(e, "Bad response received")
        raise

def prune_request(input):
    if isinstance(input, dict):
        result = {}
        for key, value in input.items():
            if value is not None:
                result[key] = prune_request(value)
        return result
    if isinstance(input, list):
        result = []
        for item in input:
            if item is not None:
                result.append(prune_request(item))
        return result
    return input

def get_headers(client_id, token=None):
    headers = {
        "x-dw-client-id": client_id,
        "content-Type": "application/json",
    }
    if token:
        headers["Authorization"] = "Bearer " + token
    return headers

class Apis:
    SHOP_CONST = "shop"
    DATA_CONST = "data"


class Demandware(object):

    def __init__(self, base_url, client_id, api=Apis.SHOP_CONST, version=None, locale=None, currency=None, pretty_print=None, callback=lambda response: response):
        self.version = version or "v18_7"
        url_suffix = "/shop/%s/" % self.version if api == Apis.SHOP_CONST else \
                     "/data/%s/sites/" % self.version if api == Apis.DATA_CONST else \
                     ""
        self.base_url = base_url + url_suffix
        self.client_id = client_id
        self.locale = locale
        self.currency = currency
        self.callback = callback
        self.pretty_print = pretty_print

    def _make_request(self, method, url, **kwargs):
        url = self._add_url_config(url)
        if kwargs.get("json"):
            kwargs["json"] = prune_request(kwargs["json"])
        result = method(url, **kwargs)
        logger.debug(
            curlify.to_curl(result.request).replace("-H 'Accept-Encoding: gzip, deflate' ", "")
        )
        if result.status_code == 204:
            return result
        #Allow a callback to be called if there is actionable data in the response that does not make it past the resource layer
        #Use a copy and don't save the result in order to preserve the original response state
        self.callback(result)
        result = check_fault(copy.copy(result))
        return result

    def _add_url_config(self, url):
        params = {}
        if self.locale:
            params['locale'] = self.locale
        if self.currency:
            params['currency'] = self.currency
        if self.pretty_print:
            params['pretty_print'] = self.pretty_print
        url_parts = urlparse.urlparse(url)
        query = dict(urlparse.parse_qsl(url_parts.query))
        query.update(params)
        url_parts = url_parts._replace(query=urlparse.urlencode(query))
        return urlparse.urlunparse(url_parts)


    def _post(self, url, token, **kwargs):
        return self._make_request(requests.post, url, headers=get_headers(client_id=self.client_id, token=token), **kwargs)


    def _get(self, url, token, **kwargs):
        return self._make_request(requests.get, url, headers=get_headers(client_id=self.client_id, token=token), **kwargs)


    def _put(self, url, token, **kwargs):
        try:
            return self._make_request(requests.put, url, headers=get_headers(client_id=self.client_id, token=token), **kwargs)
        except BaseException as ex:
            headers = get_headers(client_id=self.client_id, token=token)
            headers["x-dw-http-method-override"] = "PUT"
            return self._make_request(requests.post, url, headers=headers, **kwargs)


    def _delete(self, url, token, **kwargs):
        return self._make_request(requests.delete, url, headers=get_headers(self.client_id, token), **kwargs)


    def _patch(self, url, token, **kwargs):
        return self._make_request(requests.patch, url, headers=get_headers(client_id=self.client_id, token=token), **kwargs)


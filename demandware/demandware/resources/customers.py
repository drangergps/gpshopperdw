from demandware.models.authrequest import AuthRequest
from demandware.models.basketsresult import BasketsResult
from demandware.models.customer import Customer
from demandware.models.customeraddress import CustomerAddress
from demandware.models.customeraddressresult import CustomerAddressResult
from demandware.models.customerorderresult import CustomerOrderResult
from demandware.models.customerpaymentinstrument import CustomerPaymentInstrument
from demandware.models.customerpaymentinstrumentrequest import CustomerPaymentInstrumentRequest
from demandware.models.customerpaymentinstrumentresult import CustomerPaymentInstrumentResult
from demandware.models.customerproductlist import CustomerProductList
from demandware.models.customerproductlistitem import CustomerProductListItem
from demandware.models.customerproductlistitemresult import CustomerProductListItemResult
from demandware.models.customerproductlistresult import CustomerProductListResult
from demandware.models.customerregistration import CustomerRegistration
from demandware.models.model import Model
from demandware.models.nocontent import NoContent
from demandware.models.passwordchangerequest import PasswordChangeRequest
from demandware.models.passwordreset import PasswordReset
from demandware.resources.base import Demandware
from six.moves.urllib.parse import urlencode


class Api(Demandware):

    def post(self, input, token=None):
        """
        Registers a customer. The mandatory data are the credentials and profile last name and email.
        When using OAuth the password in the request must not be set, otherwise an InvalidPasswordException will be thrown.
        When using JWT the password is required.
        """

        # AUTO GENERATED
        CustomerRegistration(input).validate()
        result = self._post(self.base_url + '/customers'.format(**locals()), token, json=dict(input))
        return Customer(result)

    def auth_delete(self, token=None):
        """
	    Invalidates the JWT provided in the header.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/customers/auth'.format(**locals()), token)

    def auth_post(self, input, token=None):
        """
        Obtains a new JWT (JSON Web Token) for a guest or registered customer. Tokens are returned as a HTTP Authorization:Bearer response header entry. These kinds of request are supported, as specified by the type:
        Type guest - creates a new guest (non-authenticated) customer and returns a token for the customer.
        Type credentials - authenticates credentials passed in the HTTP Authorization:Basic request header, returning a token for a successfully authenticated customer otherwise results in an AuthenticationFailedException.
        Type session - authenticates the customer (anonymous or registered) on base of dwsid and dwsecuretoken cookies. It returns a token for a successfully authenticated customer, otherwise results in an AuthenticationFailedException.
        Type refresh - examines the token passed in the HTTP Authorization:Bearer request header and when valid returns a new token with an updated expiry time.
        For a request of type credentials:
        Updates profile attributes for the customer (for example, "last-visited").
        Handles the maximum number of failed login attempts.
        For a request of type session:
        Does not touch profile attributes for the registered customer (for example, "last-visited"), since this is not a real login.
        Returns different tokens for multiple requests with the same session id. Means, there should be only one call per session.
        """

        # AUTO GENERATED
        AuthRequest(input).validate()
        result = self._post(self.base_url + '/customers/auth'.format(**locals()), token, json=dict(input))
        return Customer(result)

    def get(self, customer_id, expand=None, token=None):
        """
        Gets a customer.
        """
        expand = expand or ['addresses', 'paymentinstruments']
        expand = ",".join(expand)

        args = {key: value for key, value in dict(locals()).items() if value and key not in ("customer_id", "self",)}

        # AUTO GENERATED
        result = self._get((self.base_url + '/customers/{customer_id}?' + urlencode(args)).format(**locals()), token)
        return Customer(result)

    def patch(self, customer_id, input, token=None):
        """
	    Updates a customer.
        """

        # AUTO GENERATED
        Customer(input).validate()
        result = self._patch(self.base_url + '/customers/{customer_id}'.format(**locals()), token, json=dict(input))
        return Customer(result)

    def addresses_all_get(self, customer_id, token=None):
        """
        Returns a sorted pageable list of all customer addresses in the address book. The default page size is 10
        customer addresses. The addresses are sorted so that the preferred address is always sorted first.
        The remaining addresses are sorted alphabetically by ID.
        When the customer cannot be found CustomerNotFoundException is thrown in a case of an agent but an empty result
        list is returned in a case of JWT.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/addresses'.format(**locals()), token)
        return CustomerAddressResult(result)

    def addresses_post(self, customer_id, input, token=None):
        """
	    Creates a new address with the given name for the given customer.
        """

        # AUTO GENERATED
        CustomerAddress(input).validate()
        result = self._post(self.base_url + '/customers/{customer_id}/addresses'.format(**locals()), token, json=dict(input))
        return CustomerAddress(result)

    def addresses_get(self, customer_id, address_name, token=None):
        """
        Retrieves a customer's address by address name.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/addresses/{address_name}'.format(**locals()), token)
        return CustomerAddress(result)

    def addresses_delete(self, customer_id,address_name,token=None):
        """
	    Deletes a customer's address by address name.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/customers/{customer_id}/addresses/{address_name}'.format(**locals()), token)
        return Customer(result)

    def addresses_patch(self, customer_id,address_name,input,token=None):
        """
        Updates a customer's address by address name.
        """

        # AUTO GENERATED
        CustomerAddress(input).validate()
        result = self._patch(self.base_url + '/customers/{customer_id}/addresses/{address_name}'.format(**locals()), token, json=dict(input))
        return CustomerAddress(result)

    def baskets_get(self, customer_id, token=None):
        """
        Gets the baskets of a customer.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/baskets'.format(**locals()), token)
        return BasketsResult(result)

    def orders_get(self, customer_id, token=None):
        """
        Returns a pageable list of all customer's orders. The default page size is 10.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/orders'.format(**locals()), token)
        return CustomerOrderResult(result)

    def password_put(self, customer_id, token=None):
        """
        Updates the customer's password.
        """

        # AUTO GENERATED
        result = self._put(self.base_url + '/customers/{customer_id}/password'.format(**locals()), token)
        return PasswordChangeRequest(result)

    def password_reset_post(self, input, token=None):
        """
        Starts a password reset process. A password reset token is generated and together with the resolved customer
        is passed to a afterPOST hook. The customer resolution is based on the password reset request type. Currently
        the resolution can be done by email or login. In case of an email the password reset hook is only executed if
        one and only one customer has been identified for that email. In the case that more than one customers have been
        identified for the provided email the resource does nothing.
        """

        # AUTO GENERATED
        PasswordReset(input).validate()
        result = self._post(self.base_url + '/customers/password_reset'.format(**locals()), token, json=dict(input))
        return NoContent(result)

    def payment_instruments_all_get(self, customer_id, token=None):
        """
        Gets customer payment instruments for an customer.
        Can be limited to a specific payment method by providing query parameter payment_method_id.
        When the customer cannot be found CustomerNotFoundException is thrown in a case of an agent but an empty result
        list is returned in a case of JWT.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/payment_instruments'.format(**locals()), token)
        return CustomerPaymentInstrumentResult(result)

    def payment_instruments_post(self, customer_id, input, token=None):
        """
        Adds a payment instrument to a customer information.
        """

        # AUTO GENERATED
        CustomerPaymentInstrumentRequest(input).validate()
        result = self._post(self.base_url + '/customers/{customer_id}/payment_instruments'.format(**locals()), token, json=dict(input))
        return CustomerPaymentInstrument(result)

    def payment_instruments_get(self, customer_id, payment_instrument_id, token=None):
        """
        Retrieves a customer's payment instrument by its id.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/payment_instruments/{payment_instrument_id}'.format(**locals()), token)
        return CustomerPaymentInstrument(result)

    def payment_instruments_delete(self, customer_id, payment_instrument_id, input, token=None):
        """
        Deletes a customer's payment instrument.
        """

        # AUTO GENERATED
        Model(input).validate()
        result = self._delete(self.base_url + '/customers/{customer_id}/payment_instruments/{payment_instrument_id}'.format(**locals()), token, json=dict(input))
        return Customer(result)

    def product_lists_all_get(self, customer_id, token=None):
        """
        Returns all customer product lists.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/product_lists'.format(**locals()), token)
        return CustomerProductListResult(result)

    def product_lists_post(self, customer_id, input, token=None):
        """
        Creates a customer product list.
        """

        # AUTO GENERATED
        CustomerProductList(input).validate()
        result = self._post(self.base_url + '/customers/{customer_id}/product_lists'.format(**locals()), token, json=dict(input))
        return CustomerProductList(result)

    def product_lists_get(self, customer_id, list_id, token=None):
        """
        Returns a customer product list of the given customer.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/product_lists/{list_id}'.format(**locals()), token)
        return CustomerProductList(result)

    def product_lists_patch(self, customer_id, list_id, input, token=None):
        """
        Changes a product list. Changeable properties are the name, description and if the list is public.
        """

        # AUTO GENERATED
        CustomerProductList(input).validate()
        result = self._patch(self.base_url + '/customers/{customer_id}/product_lists/{list_id}'.format(**locals()), token, json=dict(input))
        return CustomerProductList(result)

    def product_lists_delete(self, customer_id, list_id, input, token=None):
        """
        Deletes a customer product list.
        """

        # AUTO GENERATED
        Model(input).validate()
        result = self._delete(self.base_url + '/customers/{customer_id}/product_lists/{list_id}'.format(**locals()), token, json=dict(input))
        return Customer(result)

    def product_lists_items_all_get(self, customer_id, list_id, token=None):
        """
        Returns a pageable list of all items of a customer's product list. The default page size is 10.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/product_lists/{list_id}/items'.format(**locals()), token)
        return CustomerProductListItemResult(result)

    def product_lists_items_post(self, customer_id, list_id, input, token=None):
        """
        Adds an item to the customer's product list. Considered values from the request body are:
        - type: a valid type, mandatory. This is the type of the item to be added to the customer's product list.
        - priority: This is the priority of the item to be added to the customer's product list.
        - public: This is the flag whether the item to be added to the customer's product list is public.
        - product_id: a valid product id, used for product item type only. This is the id (sku) of the product related
        to the item to be added to the customer's product list. It is mandatory for product item type and it must be a
        valid product id, otherwise ProductListProductIdMissingException or ProductListProductNotFoundException will be thrown.
        - quantity: used for product item type only. This is the quantity of the item to be added to the customer's product list.
        custom properties in the form c_<CUSTOM_NAME>: the custom property must correspond to a custom attribute
        (<CUSTOM_NAME>) defined for ProductListItem. The value of this property must be valid for the
        type of custom attribute defined for ProductListItem.
        """

        # AUTO GENERATED
        CustomerProductListItem(input).validate()
        result = self._post(self.base_url + '/customers/{customer_id}/product_lists/{list_id}/items'.format(**locals()), token, json=dict(input))
        return CustomerProductListItem(result)

    def product_lists_items_get(self, customer_id, list_id, item_id, token=None):
        """
        Returns an item of a customer product list.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/customers/{customer_id}/product_lists/{list_id}/items/{item_id}'.format(**locals()), token)
        return CustomerProductListItem(result)

    def product_lists_items_delete(self, customer_id, list_id, item_id, token=None):
        """
        Removes an item from a customer product list.
        """

        # AUTO GENERATED
        Model(input).validate()
        result = self._delete(self.base_url + '/customers/{customer_id}/product_lists/{list_id}/items/{item_id}'.format(**locals()), token)
        return Customer(result)

    def product_lists_items_patch(self, customer_id, list_id, item_id, input, token=None):
        """
        Updates an item of a customer's product list. Considered values from the request body are:
        - priority: This is the priority of the customer's product list item.
        - public: This is the flag whether the customer's product list item is public.
        - quantity: used for product item type only. This is the quantity of the customer's product list item.
        - custom properties in the form c_<CUSTOM_NAME>: the custom property must correspond to a custom attribute (<CUSTOM_NAME>)
        - defined for ProductListItem. The value of this property must be valid for the type of custom attribute defined for ProductListItem.
        """

        # AUTO GENERATED
        CustomerProductListItem(input).validate()
        result = self._patch(self.base_url + '/customers/{customer_id}/product_lists/{list_id}/items/{item_id}'.format(**locals()), token, json=dict(input))
        return CustomerProductListItem(result)
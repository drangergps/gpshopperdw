from demandware.models import Slots, Slot
from demandware.resources.base import Demandware
from demandware.resources.base import build_list_args, filter_args
from six.moves.urllib.parse import urlencode


class Api(Demandware):

    # So base url stays as /data/%s
    def __init__(self,base_url,client_id,locale=None,currency=None):
        super(Api, self).__init__(base_url, client_id, api="data")

    def all_get(self, site_id, count=None, select=None, start=None, token=None, **kwargs):
        """
        Pull back slots for a given site
        """
        args = filter_args(dict(locals(), **kwargs))
        result = self._get(self.base_url + site_id + '/slots?' + urlencode(args).format(**locals()), token)
        return Slots(result)

    def  get(self, site_id, slot_id, context_type="global", select=None, token=None, **kwargs):
        """
        To access single slot resource, you construct a URL using the template shown below. This template requires
        you to specify an Id for a slot. In response, the server returns a corresponding Slot
        document.
        """

        args = filter_args(dict(locals(), **kwargs))
        result = self._get(self.base_url + site_id + '/slots/' + slot_id + '/' + context_type + "?" + urlencode(args).format(**locals()), token)

        return Slot(result)

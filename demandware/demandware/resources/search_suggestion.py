from demandware.models import SuggestionResult
from demandware.resources.base import Demandware
from demandware.utils import build_args


class Api(Demandware):

    def get(self, q="", count=5, locale=None, currency=None, token=None, **kwargs):
        """
	    Provides keyword search functionality for products, categories, content, brands and custom suggestions.
	    Returns suggested products, suggested categories, suggested content, suggested brands and custom suggestions
	    for the given search phrase.
        """
        combined_args = build_args(locals(), **kwargs)

        # AUTO GENERATED
        result = self._get(self.base_url + '/search_suggestion?%s' % (combined_args,), token)
        return SuggestionResult(result)

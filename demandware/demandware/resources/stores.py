from demandware.models import Store
from demandware.models import StoreResult
from demandware.resources.base import Demandware, build_list_args, filter_args
from six.moves.urllib.parse import urlencode


class Api(Demandware):

    def all_get(self, latitude=None, longitude=None, country_code=None, postal_code=None, distance_unit=None,
                max_distance=None, start=None, count=None,token=None, **kwargs):
        """
        This resource retrieves a list of stores, for the given site, that are within a configured distance of a
        location on the earth. The stores and their distance from the specified location are returned as a result
        set of Store objects. The distance is interpreted either in miles or kilometers depending on the
        "distance_unit" input parameter.

        The location can be specified by either directly providing a latitude/longitude coordinate pair or by providing
        a country and a postal code:

            - If a postal code is passed, the resource looks in the system's geolocation mappings to find the coordinates
            for this postal code. If no matching geolocation is found, the resource will return an empty list of stores.
            - If coordinates are passed, the values for country and postal code are ignored.
        """
        args = filter_args(dict(locals(), **kwargs))
        # AUTO GENERATED
        result = self._get((self.base_url + '/stores?' + urlencode(args)).format(**locals()), token)
        return StoreResult(result)


    def get_many(self, ids, token=None, **kwargs):
        """
        Allows to access multiple stores by a single request. This convenience resource should be used instead making
        separated requests. This saves bandwidth and CPU time on the server. The URI is the same like requesting a single
        Store by id, but multiple ids wrapped by parentheses and separated can be provided. If a parenthesis or the
        separator is part of the identifier itself it has to be URL encoded. Instead of a single Store document a
        result object of Store documents is returned. Note: Only stores that are online and active are returned.
        The maximum number of ids is 50.
        """
        args = filter_args(kwargs)
        # AUTO GENERATED
        result = self._get((self.base_url + '/stores/({id})?' + urlencode(args)).format(id=",".join(build_list_args(*ids))), token)
        return StoreResult(result)

    def get(self, id,token=None, **kwargs):
        """
        To access a store, you construct a URL using the template shown below. This template requires you to specify a
        store id. In the response, the server returns a corresponding store document.
        """
        args = filter_args(kwargs)
        # AUTO GENERATED
        result = self._get((self.base_url + '/stores/{id}?' + urlencode(args)).format(**locals()), token)
        return Store(result)
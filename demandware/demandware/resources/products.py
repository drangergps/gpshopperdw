from six.moves.urllib.parse import urlencode
from demandware.models.product import Product
from demandware.models.productresult import ProductResult
from demandware.models.shippingmethodresult import ShippingMethodResult
from demandware.resources.base import Demandware, build_list_args


class Api(Demandware):

    def get_many(self, ids, inventory_ids=None, expand=None, currency=None, locale=None, token=None):
        """
        Allows to access multiple products by a single request. This convenience resource should be used instead making
        separated requests. This saves bandwidth and CPU time on the server. The URI is the same like requesting a single
        Product by id, but multiple ids wrapped by parentheses and separated can be provided. If a parenthesis or the
        separator is part of the identifier itself it has to be URL encoded. Instead of a single Product document a
        result object of Product documents is returned. Note: Only products that are online and assigned to site
        catalog are returned. The maximum number of ids is 50.
        """
        inventory_ids = inventory_ids or []
        expand = expand or ['availability', 'images', 'prices', 'variations', 'promotions', 'links']
        expand = ",".join(expand)

        args = {key: value for key, value in dict(locals()).items() if value and key not in ("id", "self", "ids",)}

        result = self._get((self.base_url + '/products/({id})?' + urlencode(args)).format(id=",".join(build_list_args(*ids))), token)
        return ProductResult(result)

    def get(self, id, inventory_ids=None, expand=None, currency=None, locale=None, token=None):
        """
        To access single products resource, you construct a URL using the template shown below. This template requires
        you to specify an Id (typically a SKU) for a product. In response, the server returns a corresponding Product
        document, provided the product is online and assigned to site catalog. The document contains variation
        attributes (including values) and the variant matrix; this data is provided for both the master and for the variant.
        """

        inventory_ids = inventory_ids or []
        expand = expand or ['availability', 'images', 'prices', 'variations', 'promotions', 'links']
        expand = ",".join(expand)

        args = {key: value for key, value in dict(locals()).items() if value and key not in ("id", "self",)}

        result = self._get((self.base_url + '/products/{id}?' + urlencode(args)).format(**locals()), token)

        return Product(result)

    def availability_get(self, id,token=None):
        """
        Access product availability information of products that are online and assigned to site catalog.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/products/{id}/availability'.format(**locals()), token)
        return Product(result)

    def bundled_products_get(self, id,token=None):
        """
        Access bundled product information of products that are online and assigned to site catalog.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/products/{id}/bundled_products'.format(**locals()), token)
        return [Product(item) for item in result]

    def products_images_get(self, id, input,token=None):
        """
        Access product image information of products that are online and assigned to site catalog. Filter the result by
        view type and variation values.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/products/{id}/images'.format(**locals()), token)
        return Product(result)

    def links_get(self, id,token=None):
        """
        Access product link information of products that are online and assigned to site catalog.
        Filter the result by link type and link direction.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/products/{id}/links'.format(**locals()), token)
        return Product(result)

    def options_get(self, id,input,token=None):
        """
        Access product option information of products that are online and assigned to site catalog.
        """

        # AUTO GENERATED
        Product(input).validate()
        result = self._get(self.base_url + '/products/{id}/options'.format(**locals()), token, json=dict(input))
        return Product(result)

    def prices_get(self, id,input,token=None):
        """
        Access product price information of products that are online and assigned to site catalog.
        """

        # AUTO GENERATED
        Product(input).validate()
        result = self._get(self.base_url + '/products/{id}/prices'.format(**locals()), token, json=dict(input))
        return Product(result)

    def promotions_get(self, id,input,token=None):
        """
        Access product promotion information of products that are online and assigned to site catalog.
        """

        # AUTO GENERATED
        Product(input).validate()
        result = self._get(self.base_url + '/products/{id}/promotions'.format(**locals()), token, json=dict(input))
        return Product(result)

    def recommendations_get(self, id,input,token=None):
        """
        Access product recommendation information of products that are online and assigned to site catalog.
        """

        # AUTO GENERATED
        Product(input).validate()
        result = self._get(self.base_url + '/products/{id}/recommendations'.format(**locals()), token, json=dict(input))
        return Product(result)

    def set_products_get(self, id,input,token=None):
        """
        Access product set information of products that are online and assigned to site catalog.
        """

        # AUTO GENERATED
        Product(input).validate()
        result = self._get(self.base_url + '/products/{id}/set_products'.format(**locals()), token, json=dict(input))
        return Product(result)

    def shipping_methods_get(self, id,token=None):
        """
        Retrieves the applicable shipping methods for a certain product.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/products/{id}/shipping_methods'.format(**locals()), token)
        return ShippingMethodResult(result)

    def variations_get(self, id,input,token=None):
        """
        Access product variation information of products that are online and assigned to site catalog.
        """

        # AUTO GENERATED
        Product(input).validate()
        result = self._get(self.base_url + '/products/{id}/variations'.format(**locals()), token, json=dict(input))
        return Product(result)


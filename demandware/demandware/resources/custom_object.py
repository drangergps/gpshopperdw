from demandware.models import CustomObject
from demandware.resources.base import Demandware


class Api(Demandware):

    def get(self, object_type,key, token=None):
        """
        Reads a custom object with a given object type ID and a value for the key attribute of the object
        which represents its unique identifier.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/custom_objects/{object_type}/{key}'.format(**locals()), token)
        return CustomObject(result)
from demandware.models import ContentSearchResult
from demandware.resources.base import Demandware


class Api(Demandware):

    def get(self, input, token=None):
        """
        Provides keyword and refinement search functionality for content assets.
        The search result contains only content that is online and assigned to a folder.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/content_search'.format(**locals()), token, params=dict(input))
        return ContentSearchResult(result)
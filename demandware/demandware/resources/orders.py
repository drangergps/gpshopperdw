from demandware.models.basket import Basket
from demandware.models.note import Note
from demandware.models.notesresult import NotesResult
from demandware.models.order import Order
from demandware.models.orderpaymentinstrumentrequest import OrderPaymentInstrumentRequest
from demandware.models.paymentmethodresult import PaymentMethodResult
from demandware.resources.base import Demandware


class Api(Demandware):

    def post(self, input,token=None):
        """
        Submits an order based on a prepared basket.
        Note: If the basket has been submitted using Order Center (considered by it's client id) the channel type
        will be set to "Call Center". In case another channel type was set by a script before submitting the basket,
        the channel type will be reset to "Call Center" and a warning will be logged. The only considered value from
        the request body is basket_id.
        """

        # AUTO GENERATED
        Basket(input).validate()
        result = super(Api, self)._post(self.base_url + '/orders'.format(**locals()), token, json=dict(input))
        return Order(result)

    def get(self, order_no,token=None):
        """
	    Gets information for an order.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/orders/{order_no}'.format(**locals()), token)
        return Order(result)

    def patch(self, order_no,input,token=None):
        """
        Update an order.

        Considered fields for update are status (same status transitions are possible as for
        dw.order.Order.setStatus(int status)) and custom properties. During the call the correct channel type will
        be assured to be set for the order in a successful call. Without agent context the channel type will be
        storefront otherwise callcenter.
        """

        # AUTO GENERATED
        Order(input).validate()
        result = self._patch(self.base_url + '/orders/{order_no}'.format(**locals()), token, json=dict(input))
        return Order(result)

    def notes_post(self, order_no,input,token=None):
        """
        Adds a note to an existing order.
        """

        # AUTO GENERATED
        Note(input).validate()
        result = self._post(self.base_url + '/orders/{order_no}/notes'.format(**locals()), token, json=dict(input))
        return Order(result)

    def notes_get(self, order_no,token=None):
        """
        Retrieves notes for an order
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/orders/{order_no}/notes'.format(**locals()), token)
        return NotesResult(result)

    def notes_delete(self, order_no,note_id,token=None):
        """
        Removes an order note.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/orders/{order_no}/notes/{note_id}'.format(**locals()), token)
        return Order(result)

    def payment_instruments_post(self, order_no,input,token=None):
        """
        Adds a payment instrument to an order. It is possible either to supply the full payment information or only a
        customer payment instrument id and amount. In case the customer payment instrument id was set all the other
        properties (except amount) are ignored and the payment data is resolved from the stored customer payment
        information. An attempt is made to authorize the order by passing it to the authorize or
        authorizeCreditCard hook.

        Details:

        - The payment instrument is added with the provided details or the details from the customer payment instrument.
        The payment method must be applicable for the order see GET /baskets/{basket_id}/payment_methods, if the payment
        method is 'CREDIT_CARD' a payment_card must be specified in the request.

        - Order authorization:

            To authorize the order one of two possible customization hooks is called and an dw.order.OrderPaymentInstrument
            is passed as an input argument.

            Which hook is called?

                If the request includes a payment_card or the dw.order.OrderPaymentInstrument contains a creditCardType
                the customization hook dw.order.payment.authorizeCreditCard is called.
                See dw.order.hooks.PaymentHooks.authorizeCreditCard(order : Order, paymentDetails : OrderPaymentInstrument, cvn : String) : Status.
                Otherwise dw.order.payment.authorize is called. See dw.order.hooks.PaymentHooks.authorize(order : Order, paymentDetails : OrderPaymentInstrument) : Status.

            What is the dw.order.OrderPaymentInstrument input argument passed to the hook?

                If the request contains a customer_payment_instrument_id the dw.order.OrderPaymentInstrument is copied
                from the customer payment instrument (An exception is thrown if none was found).
                Otherwise the data from the request document is passed (payment_card or payment_bank_account etc. information).

        Note: the amount and the security_code (cvn) contained in the payment_card data will be propagated if available
        to dw.order.payment.authorizeCreditCard even if the dw.order.OrderPaymentInstrument is resolved from a customer payment instrument.

        Customization hook dw.ocapi.shop.order.afterPostPaymentInstrument is called. The default implementation places
        the order if the order status is CREATED and the authorization amount equals or exceeds the order total.
        Placing the order (equivalent to calling dw.order.OrderMgr.placeOrder(order : Order) in the scripting API)
        results in the order being changed to status NEW and prepared for export.
        """

        # AUTO GENERATED
        OrderPaymentInstrumentRequest(input).validate()
        result = self._post(self.base_url + '/orders/{order_no}/payment_instruments'.format(**locals()), token, json=dict(input))
        return Order(result)

    def payment_instruments_patch(self, order_no,payment_instrument_id,input,token=None):
        """
        Updates a payment instrument of an order and passes the order and updated payment instrument to the correct
        payment authorizeCreditcard or authorize hook.

        Details:

        - The payment instrument is added with the provided details or the details from the customer payment instrument.
        The payment method must be applicable for the order see GET /baskets/{basket_id}/payment_methods, if the payment
        method is 'CREDIT_CARD' a payment_card must be specified in the request.

        - Order authorization:

            To authorize the order one of two possible customization hooks is called and an dw.order.OrderPaymentInstrument
            is passed as an input argument.

            Which hook is called?

                If the request includes a payment_card or the dw.order.OrderPaymentInstrument contains a creditCardType
                the customization hook dw.order.payment.authorizeCreditCard is called.
                See dw.order.hooks.PaymentHooks.authorizeCreditCard(order : Order, paymentDetails : OrderPaymentInstrument, cvn : String) : Status.
                Otherwise dw.order.payment.authorize is called. See dw.order.hooks.PaymentHooks.authorize(order : Order, paymentDetails : OrderPaymentInstrument) : Status.

            What is the dw.order.OrderPaymentInstrument input argument passed to the hook?

                If the request contains a customer_payment_instrument_id the dw.order.OrderPaymentInstrument is copied
                from the customer payment instrument (An exception is thrown if none was found).
                Otherwise the data from the request document is passed (payment_card or payment_bank_account etc. information).

        Note: the amount and the security_code (cvn) contained in the payment_card data will be propagated if available
        to dw.order.payment.authorizeCreditCard even if the dw.order.OrderPaymentInstrument is resolved from a customer payment instrument.

        Customization hook dw.ocapi.shop.order.afterPostPaymentInstrument is called. The default implementation places
        the order if the order status is CREATED and the authorization amount equals or exceeds the order total.
        Placing the order (equivalent to calling dw.order.OrderMgr.placeOrder(order : Order) in the scripting API)
        results in the order being changed to status NEW and prepared for export.
        """


        # AUTO GENERATED
        OrderPaymentInstrumentRequest(input).validate()
        result = self._patch(self.base_url + '/orders/{order_no}/payment_instruments/{payment_instrument_id}'.format(**locals()), token, json=dict(input))
        return Order(result)

    def payment_instruments_delete(self, order_no,payment_instrument_id,token=None):
        """
        Removes a payment instrument of an order.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/orders/{order_no}/payment_instruments/{payment_instrument_id}'.format(**locals()), token)
        return Order(result)

    def payment_methods_get(self, order_no,token=None):
        """
        Gets the applicable payment methods for an existing order considering the open payment amount only.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/orders/{order_no}/payment_methods'.format(**locals()), token)
        return PaymentMethodResult(result)

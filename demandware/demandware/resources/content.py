from demandware.models import ContentResult
from demandware.resources.base import Demandware


class Api(Demandware):

    def get(self, id,input, token=None):
        """
        To access a content asset, you construct a URL using the template shown below. This template requires you to
        specify a content asset id. In response, the server returns a corresponding content asset document.
        Only content assets, which are marked as online are returned. An assignment to a folder is not necessary.
        """
        # AUTO GENERATED
        result = self._get(self.base_url + '/content/{id}'.format(**locals()), token, params=dict(input))
        return ContentResult(result)
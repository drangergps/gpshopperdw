from demandware.models.publicproductlist import PublicProductList
from demandware.models.publicproductlistitem import PublicProductListItem
from demandware.models.publicproductlistitemresult import PublicProductListItemResult
from demandware.models.publicproductlistresult import PublicProductListResult
from demandware.resources.base import Demandware


class Api(Demandware):

    def lists_get(self,token=None):
        """
        productListResultWO | /data[1]/link Retrieves all public product lists as defined by the given search term
        (email, first name, last name).
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/product_lists'.format(**locals()), token)
        return PublicProductListResult(result)

    def lists_all_get(self, list_id,token=None):
        """
        Retrieves a public product list by id.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/product_lists/{list_id}'.format(**locals()), token)
        return PublicProductList(result)

    def lists_items_all_get(self, list_id,token=None):
        """
        Retrieves the items of a public product list.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/product_lists/{list_id}/items'.format(**locals()), token)
        return PublicProductListItemResult(result)

    def lists_items_get(self, list_id,item_id,token=None):
        """
        Retrieves an item from a public product list.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/product_lists/{list_id}/items/{item_id}'.format(**locals()), token)
        return PublicProductListItem(result)

from demandware.models import Promotion
from demandware.models import PromotionResult
from demandware.resources.base import Demandware, build_list_args


class Api(Demandware):

    def all_get(self,token=None):
        """
        Handles get promotion by filter criteria Returns an array of enabled promotions matching specified filter criteria.
        In the request URL, you must provide a campaign_id parameter, and you can optionally specify a date range by
        providing start_date and end_date parameters. Both parameters are required to specify a date range: omitting
        one causes the server to return a MissingParameterException fault. Each request returns only enabled promotions;
        the server does not consider promotion qualifiers or schedules.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/promotions'.format(**locals()), token)
        return PromotionResult(result)

    def many_get(self, token=None, *ids):
        """
        Returns an array of enabled promotions for a list of specified ids. In the request URL, you can specify up
        to 50 ids. You must enclose the list of ids in parentheses. If you specify an id that contains either parentheses
        or the separator characters, you must URL encode these characters. Each request returns only enabled promotions;
        the server does not consider promotion qualifiers or schedules.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/promotions/{id}'.format(id=','.join(build_list_args(*ids))), token)
        return Promotion(result)


    def get(self, id,token=None):
        """
	    Returns an enabled promotion using a specified id. Each request returns a response only for an enabled promotion;
	    the server does not consider promotion qualifiers or schedules.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/promotions/{id}'.format(**locals()), token)
        return Promotion(result)

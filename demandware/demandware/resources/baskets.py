from demandware.models.approachingdiscountresult import ApproachingDiscountResult
from demandware.models.basket import Basket
from demandware.models.basketpaymentinstrumentrequest import BasketPaymentInstrumentRequest
from demandware.models.basketreference import BasketReference
from demandware.models.couponitem import CouponItem
from demandware.models.customerinfo import CustomerInfo
from demandware.models.giftcertificateitem import GiftCertificateItem
from demandware.models.note import Note
from demandware.models.notesresult import NotesResult
from demandware.models.orderaddress import OrderAddress
from demandware.models.paymentmethodresult import PaymentMethodResult
from demandware.models.priceadjustmentrequest import PriceAdjustmentRequest
from demandware.models.productitem import ProductItem
from demandware.models.shipment import Shipment
from demandware.models.shippingmethod import ShippingMethod
from demandware.models.shippingmethodresult import ShippingMethodResult
from demandware.resources.base import Demandware


class Api(Demandware):

    def post(self, input,token=None):
        # AUTO GENERATED
        Basket(input).validate()
        result = self._post(self.base_url + '/baskets'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def reference_post(self, input,token=None):
        """
        Creates a new basket based on a basket reference.
        The returned basket will be a copy of the basket in the reference. The basket in the reference must be a basket of an
        anonymous customer and the provided customer_id in the reference must match the anonymous customer in the basket.
        In case customer_id not matching a BasketNotFoundException will be returned as fault. All personal data like payment
        instruments and coupons will not be copied over to the new basket.
        :param input:
        :return:
        """

        # AUTO GENERATED
        BasketReference(input).validate()
        result = self._post(self.base_url + '/baskets/reference'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def get(self, basket_id, token=None):
        """
        Gets a basket.
        """
        # AUTO GENERATED
        result = self._get(self.base_url + '/baskets/{basket_id}'.format(**locals()), token)
        return Basket(result)

    def patch(self, basket_id,input,token=None):
        """
        Updates a basket. Only the currency of the basket, source code, and the custom properties of the basket and of the
        shipping items will be considered.
        """

        # AUTO GENERATED
        Basket(input).validate()
        result = self._patch(self.base_url + '/baskets/{basket_id}'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def delete(self, basket_id,token=None):
        """
        Removes a basket.
        """

        result = self._delete(self.base_url + '/baskets/{basket_id}'.format(**locals()), token)

    def approaching_discounts_get(self, basket_id,token=None):
        """
        Gets the approaching discounts of a basket
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/baskets/{basket_id}/approaching_discounts'.format(**locals()), token)
        return ApproachingDiscountResult(result)

    def billing_address_put(self, basket_id,input,token=None):
        """
        Sets the billing address of a basket.
        """

        # AUTO GENERATED
        OrderAddress(input).validate()
        result = self._put(self.base_url + '/baskets/{basket_id}/billing_address'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def coupons_post(self, basket_id, input,token=None):
        """
        Adds a coupon to an existing basket.
        """

        # AUTO GENERATED
        CouponItem(input).validate()
        result = self._post(self.base_url + '/baskets/{basket_id}/coupons'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def coupons_delete(self, basket_id,coupon_item_id,token=None):
        """
        Removes a coupon from the basket.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/baskets/{basket_id}/coupons/{coupon_item_id}'.format(**locals()), token)
        return Basket(result)

    def customer_put(self, basket_id,input,token=None):
        """
        Sets customer information for an existing basket.
        """

        # AUTO GENERATED
        CustomerInfo(input).validate()
        result = self._put(self.base_url + '/baskets/{basket_id}/customer'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def gift_certificate_items_post(self, basket_id,input,token=None):
        """
        Adds a gift certificate item to an existing basket.
        """

        # AUTO GENERATED
        GiftCertificateItem(input).validate()
        result = self._post(self.base_url + '/baskets/{basket_id}/gift_certificate_items'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def gift_certificate_items_delete(self, basket_id,gift_certificate_item_id,token=None):
        """
        Deletes a gift certificate item from an existing basket.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/baskets/{basket_id}/gift_certificate_items/{gift_certificate_item_id}'.format(**locals()), token)
        return Basket(result)

    def gift_certificate_items_patch(self, basket_id,gift_certificate_item_id,input,token=None):
        # AUTO GENERATED
        GiftCertificateItem(input).validate()
        result = self._patch(self.base_url + '/baskets/{basket_id}/gift_certificate_items/{gift_certificate_item_id}'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def items_post(self, basket_id, input, token=None):
        """
        Adds a new item to a basket.
        The added item is associated with the specified shipment. If no shipment id is specified, the added item is associated
        with the default shipment.
        """

        # AUTO GENERATED
        assert isinstance(input, list)
        for product in input:
            ProductItem(product).validate()
        result = self._post(self.base_url + '/baskets/{basket_id}/items'.format(**locals()), token, json=input)
        return Basket(result)

    def items_patch(self, basket_id,item_id,input,token=None):
        """
        Updates an item in a basket.
        """

        # AUTO GENERATED
        ProductItem(input).validate()
        result = self._patch(self.base_url + '/baskets/{basket_id}/items/{item_id}'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def items_delete(self, basket_id,item_id,token=None):
        """
        Removes a product item from the basket.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/baskets/{basket_id}/items/{item_id}'.format(**locals()), token)
        return Basket(result)

    def notes_post(self, basket_id,input,token=None):
        """
	    Adds a note to an existing basket.
        """

        # AUTO GENERATED
        Note(input).validate()
        result = self._post(self.base_url + '/baskets/{basket_id}/notes'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def notes_get(self, basket_id,token=None):
        """
        Retrieves notes for a basket.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/baskets/{basket_id}/notes'.format(**locals()), token)
        return NotesResult(result)

    def notes_delete(self, basket_id,note_id,token=None):
        """
        Removes a basket note.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/baskets/{basket_id}/notes/{note_id}'.format(**locals()), token)
        return Basket(result)

    def payment_instruments_post(self, basket_id,input,token=None):
        """
	    Adds a payment instrument to a basket.
        Payment instruments are usually authorized after order creation, for example in a custom hook. The default
        payment authorization process executes an authorization when a payment instrument is added to an order or
        updated.
        See POST /orders/{order_no}/payment_instruments and PATCH /orders/{order_no}/payment_instruments/{payment_instrument_id}.
        NOTE: If CREDIT_CARD is selected as the payment_method_id, it is mandatory to provide the property card_type.
        """

        # AUTO GENERATED
        BasketPaymentInstrumentRequest(input).validate()
        result = self._post(self.base_url + '/baskets/{basket_id}/payment_instruments'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def payment_instruments_patch(self, basket_id,payment_instrument_id,input,token=None):
        """
        Updates a payment instrument of a basket.
        Payment instruments are usually authorized after order creation, for example in a custom hook. The default
        payment authorization process executes an authorization when a payment instrument is added to an order or updated.
        See POST /orders/{order_no}/payment_instruments and PATCH /orders/{order_no}/payment_instruments/{payment_instrument_id}
        """

        # AUTO GENERATED
        BasketPaymentInstrumentRequest(input).validate()
        result = self._patch(self.base_url + '/baskets/{basket_id}/payment_instruments/{payment_instrument_id}'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def payment_instruments_delete(self, basket_id,payment_instrument_id,token=None):
        """
        Removes a payment instrument of a basket.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/baskets/{basket_id}/payment_instruments/{payment_instrument_id}'.format(**locals()), token)
        return Basket(result)

    def payment_methods_get(self, basket_id,token=None):
        """
        Gets applicable payment methods for an existing basket considering the open payment amount only.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/baskets/{basket_id}/payment_methods'.format(**locals()), token)
        return PaymentMethodResult(result)

    def price_adjustments_post(self, basket_id,input,token=None):
        """
        Adds a custom manual price adjustment to the basket.
        """

        # AUTO GENERATED
        PriceAdjustmentRequest(input).validate()
        result = self._post(self.base_url + '/baskets/{basket_id}/price_adjustments'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def price_adjustments_delete(self, basket_id,price_adjustment_id,token=None):
        """
        Removes a custom manual price adjustment from the basket.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/baskets/{basket_id}/price_adjustments/{price_adjustment_id}'.format(**locals()), token)
        return Basket(result)

    def shipments_post(self, basket_id,input,token=None):
        """
        	Creates a new shipment for a basket.
            The created shipment is initialized with values provided in the body document and can be updated with
            further data API calls. Considered from the body are the following properties if specified:
            - the id
            - the shipping address
            - the shipping method
            - gift boolean flag
            - gift message
            - custom properties
        """

        # AUTO GENERATED
        Shipment(input).validate()
        result = self._post(self.base_url + '/baskets/{basket_id}/shipments'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def shipments_patch(self, basket_id,shipment_id,input,token=None):
        """
        Updates a shipment for a basket.
        The shipment is initialized with values provided in the body document and can be updated with further data API
        calls. Considered from the body are the following properties if specified
        - the id
        - the shipping address
        - the shipping method
        - gift boolean flag
        - gift message
        - custom properties
        """

        # AUTO GENERATED
        Shipment(input).validate()
        result = self._patch(self.base_url + '/baskets/{basket_id}/shipments/{shipment_id}'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def shipments_delete(self, basket_id,shipment_id,token=None):
        """
        Removes a specified shipment and all associated product, gift certificate, shipping and price adjustment line
        items from a basket. It is not permissible to remove the default shipment.
        """

        # AUTO GENERATED
        result = self._delete(self.base_url + '/baskets/{basket_id}/shipments/{shipment_id}'.format(**locals()), token)
        return Basket(result)

    def shipments_shipping_address_put(self, basket_id,shipment_id,input,token=None):
        """
        Sets a shipping address of a specific shipment of a basket.
        """

        # AUTO GENERATED
        OrderAddress(input).validate()
        result = self._put(self.base_url + '/baskets/{basket_id}/shipments/{shipment_id}/shipping_address'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def shipments_shipping_method_put(self, basket_id,shipment_id,input,token=None):
        """
        Sets a shipping method to a specific shipment of a basket.
        """

        # AUTO GENERATED
        ShippingMethod(input).validate()
        result = self._put(self.base_url + '/baskets/{basket_id}/shipments/{shipment_id}/shipping_method'.format(**locals()), token, json=dict(input))
        return Basket(result)

    def shipments_shipping_methods_get(self, basket_id,shipment_id,token=None):
        """
        Gets the applicable shipping methods for a certain shipment of a basket
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/baskets/{basket_id}/shipments/{shipment_id}/shipping_methods'.format(**locals()), token)
        return ShippingMethodResult(result)

    def storefront_basket_put(self, basket_id, exchange=True, token=None):
        """
        Marks a basket as storefront basket.
        """

        result = self._put(self.base_url + '/baskets/{basket_id}/storefront?exchange={exchange}'.format(**locals()), token)
        return Basket(result)

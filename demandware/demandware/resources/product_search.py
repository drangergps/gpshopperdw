from demandware.models import ProductSearchResult
from demandware.utils import build_args
from demandware.resources.base import Demandware


class Api(Demandware):

    def get(self, q="", count=100, expand="availability,images,prices,variations", sort="", start=0, locale=None, currency=None, token=None, **kwargs):
        """
        Provides keyword and refinement search functionality for products. Only returns the product id, link and
        name in the product search hit. Other search hit properties can be added by using the expand parameter.
        The search result contains only products that are online and assigned to site catalog.
        """

        combined_args = build_args(locals(), **kwargs)

        # AUTO GENERATED
        result = self._get(self.base_url + '/product_search?%s' % (combined_args,), token)
        return ProductSearchResult(result)

    def availability_get(self,token=None):
        """
        Provides keyword and refinement search functionality for products. Only returns the product id, link,
        name and availability information in the product search hit.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/product_search/availability'.format(**locals()), token)
        return ProductSearchResult(result)

    def images_get(self,token=None):
        """
        Provides keyword and refinement search functionality for products. Only returns the product id, link,
        ame and image information in the product search hit.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/product_search/images'.format(**locals()), token)
        return ProductSearchResult(result)

    def prices_get(self,token=None):
        """
        Provides keyword and refinement search functionality for products. Only returns the product id, link,
        name and price information in the product search hit.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/product_search/prices'.format(**locals()), token)
        return ProductSearchResult(result)

    def variations_get(self,token=None):
        """
        Provides keyword and refinement search functionality for products. Only returns the product id, name and
        variation information in the product search hit.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/product_search/variations'.format(**locals()), token)
        return ProductSearchResult(result)


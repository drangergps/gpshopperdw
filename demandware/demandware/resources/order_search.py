from demandware.models.ordersearchrequest import OrderSearchRequest
from demandware.resources.base import Demandware


class Api(Demandware):

    def order_search_post(self, input,token=None):
        """
        Searches for orders.
        The query attribute specifies a complex query that can be used to narrow down the search.
        Note that search fields are mandatory now and no default ones are supported.
        As the old order search version, the new one always uses Search Service too and the for that reason Order Incremental Indexing should be enabled. Otherwise HTTP 500 response will occur.
        The supported search fields are:
        - affiliate_partner_i_d
        - affiliate_partner_name
        - business_type
        - channel_type
        - confirmation_status
        - created_by
        - creation_date
        - currency_code
        - customer_email
        - customer_name
        - customer_no
        - export_after
        - export_status
        - external_order_no
        - external_order_status
        - last_modified
        - order_no
        - original_order_no
        - payment_status
        - replaced_order_no
        - replacement_order_no
        - shipping_status
        - status
        - total_gross_price
        - total_net_price
        - order.has_holds
        - coupon_line_items.coupon_code
        - coupon_line_items.coupon_id
        - holds.type
        - invoices.status
        - order_items.status
        - payment_instruments.credit_card_type
        - payment_instruments.payment_method_id
        - product_items.product_id
        - return_cases.return_case_number
        - shipments.shipping_method_id
        - shipping_orders.shipping_order_number

        The sort order of the retrieved orders could be specified by the "sorts" parameter. It is a list of objects
        presenting field name and sort direction ("asc" or "desc").
        Custom attributes could be used as search fields and as sort fields too. A prefix "c_" has to be added to them.
        """


        # AUTO GENERATED
        OrderSearchRequest(input).validate()
        result = self._post(self.base_url + '/order_search'.format(**locals()), token, json=dict(input))
        return OrderSearchResult(result)

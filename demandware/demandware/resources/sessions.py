from demandware.resources.base import Demandware

class Api(Demandware):

    def post(self, token=None):
        """
        Exchanges a JWT token into a new session. If the given token is valid, creates a new session, which is associated
        with the authenticated or anonymous customer. All Set-Cookie headers for handling the session are applied on the response.

        When a session ID is sent in with the request, the specified session is ignored. Only the incoming JWT token
        is used to create a new session.
        """

        # AUTO GENERATED
        result = self._post(self.base_url + '/sessions'.format(**locals()), token)
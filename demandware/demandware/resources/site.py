from demandware.models import Site
from demandware.resources.base import Demandware


class Api(Demandware):

    def get(self, token=None):
        """
        Access site information, like site status and site content URLs.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/site'.format(**locals()), token)
        return Site(result)
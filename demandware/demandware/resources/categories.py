from demandware.models import Category
from demandware.models import CategoryResult
from demandware.resources.base import Demandware, build_list_args


class Api(Demandware):

    def get_many(self, ids, token=None, **kwargs):
        """
        When you use the URL template below, the server returns multiple categories (a result object of category
        documents). You can use this template as a convenient way of obtaining multiple categories in a single
        request, instead of issuing separate requests for each category. You can specify multiple ids
        (up to a maximum of 50). You must enclose the list of ids in parentheses. If a category identifier contains
        a parenthesis or the separator sign, you must URL encode the character. The server only returns online categories.
        """

        result = self._get(self.base_url + '/categories/({id})'.format(id=",".join(build_list_args(*ids))), token, params=kwargs)
        return CategoryResult(result)


    def get(self, id, token=None, **kwargs):
        """
        When you use the URL template below, the server returns a category identified by its id; by default,
        the server also returns the first level of subcategories, but you can specify another level by setting the
        levels parameter. The server only returns online categories.
        """

        # AUTO GENERATED
        result = self._get(self.base_url + '/categories/{id}'.format(**locals()), token, params=kwargs)
        return Category(result)

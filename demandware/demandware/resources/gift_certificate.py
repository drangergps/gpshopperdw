from demandware.resources.base import Demandware


class Api(Demandware):

    def gift_certificate_post(self, input,token=None):
        """
        Action to retrieve an existing gift certificate.
        """

        # AUTO GENERATED
        return self._post(self.base_url + '/gift_certificate'.format(**locals()), token, json=dict(input))